package util;

public interface Visitable {
    public void commit(Visitor v);
}
