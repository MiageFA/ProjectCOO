package util;

import bdd.DO.Coordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CalculatePaths {
	
	/**calcule les coordonnées des cases suivantes directe par rapport au depart (dep), sur le chemin le plus court
	 * ex: suiv((1,1),(3,3)) => [(1,2),(2,1)]
	 * ex: suiv((3,1),(3,3)) => [(3,2)]
	 * @param dep les coordonnees de depart
	 * @param arr les coordonnees d'arrivee
	 * @return les coordonnes des points suivants 
	 */
	private static List<Coordinate> suiv(Coordinate dep, Coordinate arr) {
		List<Coordinate> list =new ArrayList<>();
		if(dep.getX()<arr.getX()){
			int x = dep.getX()+1;
			Coordinate c = new Coordinate(x,dep.getY());
			list.add(c);
		}
		if(dep.getY()<arr.getY()){
			int y = dep.getY()+1;
			Coordinate c = new Coordinate(dep.getX(),y);
			list.add(c);
		}
		if(dep.getX()>arr.getX()){
			int x = dep.getX()-1;
			Coordinate c = new Coordinate(x,dep.getY());
			list.add(c);
		}
		if(dep.getY()>arr.getY()){
			int y = dep.getY()-1;
			Coordinate c = new Coordinate(dep.getX(),y);
			list.add(c);
		}
		
		return list;
	}
	
	/**
	 * copie la liste de coordonnees donnee en parametre
	 * @param coordinates :  les coordonnees a copier
	 * @return la nouvelle liste (copie)
	 */
	private  static List<Coordinate> clone (List<Coordinate> coordinates){
		List<Coordinate> copy=new ArrayList<>();
		for(Coordinate c : coordinates){
			copy.add(c);
		}
		return copy;
	}
	
	/**
	 * 
	 * @param dep : les coordonnees de depart
	 * @param arr : les coordonnees d'arrivee
	 * @param list: la liste qui va contenir les paths possible
	 * @return les chemins finaux et l'historique de calcule 
	 */
	private  static List<List<Coordinate>> paths(Coordinate dep , Coordinate arr, List<List<Coordinate>> list){
		List<Coordinate> suiv = suiv(dep,arr);
		if(suiv.isEmpty()){
			return list;
		}
		for(Coordinate c:suiv){
			List<List<Coordinate>> newList=new ArrayList<>();
			for(List<Coordinate> prec: list){
				
				if(dep.equals(prec.get(prec.size()-1))){
					List<Coordinate> copy = clone(prec);
					copy.add(c);
					newList.add(copy);
				}
			}
			list.addAll(newList);
			paths(c,arr,list);
		}
		return list;
	}
	
	/**
	 * filtre la liste on prenant que les chemins finaux et en enlevant les doublons
	 * @param dep: les coordonnees de depart
	 * @param arr: les coordonnees d'arrivee
	 * @param list: la list des chemins et de leurs historique (retourner par paths(dep,arr,[dep]))
	 * @return
	 */
	private static  List<List<Coordinate>> filter (Coordinate dep,Coordinate arr,List<List<Coordinate>> list){
		List<List<Coordinate>> resultList = new ArrayList<>();
		int distance = Math.abs(arr.getX()-dep.getX()) + Math.abs(arr.getY()-dep.getY());
		for(List<Coordinate> l : list){
			if(l.size()==distance+1){
				resultList.add(l);
			}
		}
		return resultList.stream().distinct().collect(Collectors.toList());
		
	}
	
	/**
	 * calculates all shortest paths possible
	 * @param dep : the {@link Coordinate} of the start case
	 * @param arr : the {@link Coordinate} of the end case
	 * @return a list paths (which is a list of coordinates)
	 */
	public static List<List<Coordinate>> calculatePaths(Coordinate dep,Coordinate arr){
		List<List<Coordinate>> list = new ArrayList<>();
		List<Coordinate> l = new ArrayList<>();
		l.add(dep);
		list.add(l);
		return filter(dep,arr,paths(dep,arr,list));
	}
	
	
	public static void main(String[] args) {

		Coordinate dep = new Coordinate(3,3);
		Coordinate arr = new Coordinate(1 ,1);
		List<List<Coordinate>> result = calculatePaths(dep, arr);
		System.out.println(result);
		System.out.println(result.size());
		
	}
}
