package util;

/**
 * Game state
 */
public enum State {
    IN_PROGRESS,
    FINISHED,
    WAITING
}
