package util;

import model.ADomainObject;

public interface Observer {
    void update(ADomainObject aDomainObject);
}
