package util.UnitOfWork;

import model.ADomainObject;
import util.Observer;
import util.commiter.Commiter;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton
 * Use of observer pattern
 */
public class UnitOfWork implements Observer {

    private static UnitOfWork unitOfWork = null;
    private List<ADomainObject> dirtyDomainObjects;

    public UnitOfWork() {
        this.dirtyDomainObjects = new ArrayList<>();
    }

    public static UnitOfWork getInstance() {
        if (unitOfWork == null)
            unitOfWork = new UnitOfWork();
        return unitOfWork;
    }

    public void update(ADomainObject aDomainObject)
    {
        this.dirtyDomainObjects.add(aDomainObject);
    }

    public void commit() {
        Commiter commiter = new Commiter();
        dirtyDomainObjects.forEach(x -> {
            commiter.commit(x);
        });
        clear();
    }

    public void clear() {
        dirtyDomainObjects.clear();
    }
}
