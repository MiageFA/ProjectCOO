package util;

public interface Visitor {
    void commit(Visitable v);
    void destroy(Visitable v);
    void create(Visitable v);
}
