package util.commiter;

import util.Visitable;
import util.Visitor;

/**
 * Use of visitor pattern
 */
public class Commiter implements Visitor {
    public Commiter() {
    }

    @Override
    public void commit(Visitable v) {
        v.commit(this);
    }

    @Override
    public void destroy(Visitable v) {

    }

    @Override
    public void create(Visitable v) {

    }
}
