package model;

import util.Observable;
import util.Observer;
import util.UnitOfWork.UnitOfWork;
import util.Visitable;
import util.Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of all entities of game
 * Implements Observable and Visitable patterns
 * Visitable -> unitOfWork use it to dispatch commit between datamappers
 * Observable -> unitOfWork use it to add it to dirtyObjectList
 *            -> Display use it to redisplay object after it change
 */
public abstract class ADomainObject implements Observable, Visitable {
    protected Integer id;
    private List<Observer> observerList = new ArrayList<>();

    public ADomainObject() {

        observerList.add(UnitOfWork.getInstance());
    }

    public void addObserver(Observer observer) {
        if (!observerList.contains(observer))
            observerList.add(observer);
    }

    public void removeObserver(Observer observer) {
        if (observerList.contains(observer))
            observerList.remove(observer);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void notifyAllObservers() {
        observerList.forEach(observer -> observer.update(this));
    }

    public void notifyDestruction()
    {
        observerList.forEach(observer -> observer.update((this)));
    }

    public void commit(Visitor v){}

}
