package display.connection.listener;

import display.Display;
import display.connection.LoginWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginButtonClickListener implements ActionListener{
	Display connectionWindow;
	public LoginButtonClickListener(Display connectionWindow) {
		this.connectionWindow=connectionWindow;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		connectionWindow.dispose();
		new LoginWindow();
	}

}
