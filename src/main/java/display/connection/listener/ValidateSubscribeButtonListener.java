package display.connection.listener;

import bdd.DO.Player;
import controller.form.NewPlayerForm;
import display.connection.SubscribePanel;
import service.connexionService.SubscribeService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ValidateSubscribeButtonListener implements ActionListener{
	
	private SubscribePanel subscribePanel;
	
	public ValidateSubscribeButtonListener(final SubscribePanel panel) {
		this.subscribePanel=panel;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
        NewPlayerForm newPlayerForm = new NewPlayerForm();

        newPlayerForm.setFirstName(subscribePanel.getFirstNameField().getText());
        newPlayerForm.setLastName(subscribePanel.getLastNameField().getText());
        newPlayerForm.setNickName(subscribePanel.getLoginField().getText());
        newPlayerForm.setPassword(subscribePanel.getPasswordField().getText());

        SubscribeService subscribeService = SubscribeService.getSubscribeService();
        Player player = SubscribeService.getSubscribeService().subscribe(newPlayerForm);
        
        //appeler le gamedisplay
		
	}
	
}
