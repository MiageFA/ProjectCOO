package display.connection.listener;

import display.Display;
import display.connection.SubscribePanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SubscribeButtonListner implements ActionListener{
	
	private Display connectionWindow;
	
	public SubscribeButtonListner(Display connectionWindow) {
		this.connectionWindow=connectionWindow;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		 connectionWindow.setContentPane(new SubscribePanel(connectionWindow) );
		 SwingUtilities.updateComponentTreeUI(connectionWindow);
		 
		
	}

}
