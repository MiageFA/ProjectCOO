package display.connection.listener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.JTextField;

import bdd.DO.Game;
import bdd.DO.Player;
import bdd.DO.Round;
import display.connection.LoginWindow;
import display.game.GameWindow;
import service.RoundCalculatorService;
import service.connexionService.ConnectionService;
import service.gameService.GameService;
import util.State;

public class LoginButtonListener implements ActionListener {
	final Logger LOGGER = Logger.getGlobal();
	private LoginWindow connectionWindow;
	private JTextField loginField;

	public LoginButtonListener(final JTextField loginField, LoginWindow connectionWindow) {
		this.loginField = loginField;
		this.connectionWindow = connectionWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LOGGER.info(loginField.getText() + " try to connect");
		Player player = ConnectionService.connect(loginField.getText());

		if (player == null) {
			connectionWindow.getMessageLabel().setText("authentification error");
			connectionWindow.getMessageLabel().setForeground(Color.RED);
			
		} else {
			String playerString = player.toString();

			LOGGER.info("get player : " + playerString + " on trying to connect " + loginField.getText());

			connectionWindow.dispose();

			List<Game> games = player.getGames().stream().filter(game ->
						State.IN_PROGRESS.equals(game.getState())).collect(Collectors.toList());
			Game game = null;
			if (!(games == null || games.isEmpty())) {
				game = games.get(0);
			}
			if (game != null) {
				new GameWindow(game, player);
			} else {
				new GameWindow(player);
			}
		}

	}

}
