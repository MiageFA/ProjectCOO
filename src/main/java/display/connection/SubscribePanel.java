package display.connection;

import display.Display;
import display.connection.listener.LoginButtonClickListener;
import display.connection.listener.ValidateSubscribeButtonListener;
import service.connexionService.ConnectionService;

import javax.swing.*;
import java.awt.*;

public class SubscribePanel extends JPanel {
    JTextField loginField;
    JTextField firstNameField;
    JTextField lastNameField;
    JTextField passwordField;
    private ConnectionService connexionService;
    private Display connectionWindow;

    public SubscribePanel(final Display connectionWindow) {
        this.connectionWindow=connectionWindow;
        connexionService = ConnectionService.getConnexionService();
        this.setLayout(new GridLayout(6, 1));
        
        loginField = new JTextField();
        firstNameField = new JTextField();
        lastNameField = new JTextField();
        passwordField = new JTextField();
        
        JPanel loginPanel=new JPanel(new GridLayout(1,2));
        JPanel firstNamePanel = new JPanel(new GridLayout(1,2));
        JPanel lastNamePanel =new JPanel(new GridLayout(1,2));
        JPanel passwordPanel = new JPanel(new GridLayout(1,2));
        
        JLabel loginLabel = new JLabel("Login");
        JLabel lastNameLabel = new JLabel("last name");
        JLabel firstNameLabel = new JLabel("First name");
        JLabel passwordLabel = new JLabel("password");
        
        loginPanel.add(loginLabel);
        loginPanel.add(loginField);
        
        firstNamePanel.add(firstNameLabel);
        firstNamePanel.add(firstNameField);
        
        lastNamePanel.add(lastNameLabel);
        lastNamePanel.add(lastNameField);
        
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);
        
        JButton validateButton = new JButton("Valider");
        validateButton.addActionListener(new ValidateSubscribeButtonListener(this));
        
        JButton loginButton = new JButton("se connecter");
        loginButton.addActionListener(new LoginButtonClickListener(connectionWindow));
        
        this.add(loginPanel);
        this.add(firstNamePanel);
        this.add(lastNamePanel);
        this.add(passwordPanel);
        this.add(validateButton);
        this.add(loginButton);
        setVisible(true);
    }

	public JTextField getLoginField() {
		return loginField;
	}

	public void setLoginField(JTextField loginField) {
		this.loginField = loginField;
	}

	public JTextField getFirstNameField() {
		return firstNameField;
	}

	public void setFirstNameField(JTextField firstNameField) {
		this.firstNameField = firstNameField;
	}

	public JTextField getLastNameField() {
		return lastNameField;
	}

	public void setLastNameField(JTextField lastNameField) {
		this.lastNameField = lastNameField;
	}

	public JTextField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JTextField passwordField) {
		this.passwordField = passwordField;
	}
    
    
}
