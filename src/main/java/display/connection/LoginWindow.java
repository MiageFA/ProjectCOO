package display.connection;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import display.Display;
import display.connection.listener.LoginButtonListener;
import display.connection.listener.SubscribeButtonListner;

public class LoginWindow extends Display {
	private JTextField loginField;
	private JLabel messageLabel;

    public LoginWindow() {
        super("Connexion");
        this.setSize(400, 150);
        this.messageLabel=new JLabel();
        JPanel container = new JPanel(new GridLayout(4, 1));
        
        //login
        JPanel loginPanel = new JPanel(new GridLayout(1,2));
        JLabel loginLabel = new JLabel ("Login");
        loginField=new JTextField("");
        loginPanel.add(loginLabel);
        loginPanel.add(loginField);
        
        container.add(loginPanel,BorderLayout.NORTH);
        //buttons 
        JPanel buttonsPanel = new JPanel(new GridLayout(1,2));
        JButton loginButton = new JButton("Se connecter");
        JButton subscribeButton= new JButton("S'inscrire");
        loginButton.setSize(20,60);
        subscribeButton.setSize(20,60);
        
        loginButton.addActionListener(new LoginButtonListener(loginField,this));
        subscribeButton.addActionListener(new SubscribeButtonListner(this));
        
        buttonsPanel.add(loginButton);
        buttonsPanel.add(subscribeButton);
        
        container.add(buttonsPanel);
        container.add(this.messageLabel);
        
        setContentPane(container);
        setVisible(true);
    }

	public JTextField getLoginField() {
		return loginField;
	}

	public void setLoginField(JTextField loginField) {
		this.loginField = loginField;
	}

	public JLabel getMessageLabel() {
		return messageLabel;
	}

	public void setMessageLabel(JLabel messageLabel) {
		this.messageLabel = messageLabel;
	}
    
    
}
