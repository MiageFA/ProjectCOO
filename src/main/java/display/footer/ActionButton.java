package display.footer;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import controller.display.ActionController;
import display.footer.listener.ActionButtonListener;
import display.game.listener.GlobalListenerService;

import javax.swing.*;

public class ActionButton extends JPanel {
    private ActionController action;
    private ActionButtonListener actionButtonListener;
    private Game game;
    private JButton button;
    private Player player;
    private GlobalListenerService globalListenerService;

    public ActionButton(ActionController action, Game game, Player player, String title) {
        super();
        this.action = action;
        this.game = game;
        this.player = player;
        setContent(title);
        setVisible(true);

        globalListenerService = GlobalListenerService.getInstance();
    }

    public void setContent(String title) {
        button = new JButton(title);
        actionButtonListener = new ActionButtonListener(action, game, player);
        button.addActionListener(actionButtonListener);
        button.setEnabled(false);
        add(button);
    }

    public ActionButtonListener getActionButtonListener() {
        return actionButtonListener;
    }

    public void setVisibility(Coordinate coordinate, Object param) {
        button.setEnabled(!globalListenerService.isActionComplete() && (coordinate != null && action.canExecute(game, coordinate, player, param)));
    }
}
