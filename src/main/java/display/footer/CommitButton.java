package display.footer;

import display.footer.listener.CommitListener;

import javax.swing.*;

public class CommitButton extends JPanel {
    private JButton commit;

    public CommitButton(String title)
    {
        super();

        setContent(title);
        setVisible(true);
    }

    public void setContent(String title)
    {
        commit = new JButton(title);
        commit.addActionListener(new CommitListener());
        add(commit);
    }

    public void setVisibility(boolean visibility)
    {
        commit.setEnabled(visibility);
    }
}
