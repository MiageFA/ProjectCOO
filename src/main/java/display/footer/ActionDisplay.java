package display.footer;

import bdd.DO.Game;
import bdd.DO.Player;
import controller.display.CreateArmyController;
import controller.display.CreateCityController;
import controller.display.DestroyCityController;
import controller.display.MoveArmyController;
import display.game.displayObserver.DisplayObserver;
import display.game.listener.GlobalListenerService;

import javax.swing.*;
import java.awt.*;


/**
 *  Display action buttons
 *  Implements DisplayObserver interface
 */
public class ActionDisplay extends JPanel implements DisplayObserver{
    ActionButton createCity;
    ActionButton destroyCity;
    ActionButton moveArmy;
    ActionButton createArmy;
    CommitButton commitButton;
    GlobalListenerService globalListenerService;

    public ActionDisplay(Game game, Player player)
    {
        super(new GridLayout(1, 5));
        setContent(game, player);
        setVisible(true);

        globalListenerService = GlobalListenerService.getInstance();
        globalListenerService.addObserver(this);
    }

    public void setContent(Game game, Player player)
    {
        createCity = new ActionButton(CreateCityController.getInstance(), game, player, "Créer ville");
        destroyCity = new ActionButton(DestroyCityController.getInstance(), game, player, "Détruire ville");
        createArmy = new ActionButton(CreateArmyController.getInstance(), game, player, "Créer armée");
        moveArmy = new ActionButton(MoveArmyController.getInstance(), game, player, "Déplacer armée");
        commitButton = new CommitButton("Valider");

        add(createCity);
        add(destroyCity);
        add(createArmy);
        add(moveArmy);
        add(commitButton);
    }

    public ActionButton getCreateCity() {
        return createCity;
    }

    public ActionButton getDestroyCity() {
        return destroyCity;
    }

    public ActionButton getMoveArmy() {
        return moveArmy;
    }

    public ActionButton getCreateArmy() {
        return createArmy;
    }

    @Override
    public void update() {
        createCity.setEnabled(false);
        destroyCity.setEnabled(false);
        createArmy.setEnabled(false);
        moveArmy.setEnabled(false);
    }
}
