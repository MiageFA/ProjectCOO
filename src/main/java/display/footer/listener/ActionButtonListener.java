package display.footer.listener;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import controller.display.ActionController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionButtonListener implements ActionListener {
    private ActionController action;
    private Player player;
    private Game game;
    private Coordinate coordinate;
    private Object param;

    public ActionButtonListener(ActionController action, Game game, Player player) {
        this.action = action;
        this.game = game;
        this.player = player;
        this.param = null;
        this.coordinate = null;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (action.canExecute(game, coordinate, player, param))
            action.execute(game, coordinate, player, param);
    }
}
