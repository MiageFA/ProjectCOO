package display.footer.listener;

import controller.display.CommitController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CommitListener implements ActionListener {
    private CommitController commitController;

    public CommitListener() {
        commitController = CommitController.getInstance();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        commitController.commit();
    }
}
