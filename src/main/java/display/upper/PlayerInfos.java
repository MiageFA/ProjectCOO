package display.upper;

import bdd.DO.Game;
import bdd.DO.Player;
import model.ADomainObject;
import util.Observer;

import javax.swing.*;
import java.awt.*;

public class PlayerInfos extends JPanel implements Observer{
    Game game;
    Player player;
    JLabel name;
    JLabel nbRessources;
    JLabel nbArmies;
    JLabel nbCities;
    JLabel gameName;

    public PlayerInfos(Game game, Player player)
    {
        super(new GridLayout(1, 5));
        this.game = game;
        this.player = player;
        player.addObserver(this);
        name = new JLabel();
        nbRessources = new JLabel();
        nbArmies = new JLabel();
        nbCities = new JLabel();
        gameName = new JLabel();
        setLabels();
        setVisible(true);
    }

    public void setLabels()
    {
        name.setText("Pseudo : " + player.getPseudo());
        nbRessources.setText("Ressources : " + ((game.getResources().get(player) == null) ? (0) : (game.getResources().get(player))));
        nbArmies.setText("Armées : " + player.getArmyList().size());
        nbCities.setText("Villes : " + player.getCityList().size());
        gameName.setText("Partie : " + game.getName());
        add(name);
        add(nbRessources);
        add(nbArmies);
        add(nbCities);
        add(gameName);
    }

    @Override
    public void update(ADomainObject aDomainObject) {
        System.out.println("update");
        setLabels();
    }
}
