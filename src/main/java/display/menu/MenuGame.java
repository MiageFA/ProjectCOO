package display.menu;

import javax.swing.JMenu;

import bdd.DO.Player;
import display.game.GameWindow;
import display.menu.item.ChangeGameItem;
import display.menu.item.CreateGameItem;
import display.menu.item.JoinGameItem;
import display.menu.item.LaunchGameItem;

public class MenuGame extends JMenu {

    GameWindow window;
    Player player;
    public MenuGame(GameWindow window,Player player) {
        super("game");
        this.window=window;
        this.player=player;
        this.add(new CreateGameItem(player, window));
        this.add(new ChangeGameItem(player, window));
        this.add(new JoinGameItem(player, window));
        this.add(new LaunchGameItem(player, window));
    
    }
    

}
