package display.menu;

import bdd.DO.Player;
import display.game.GameWindow;

import javax.swing.*;

public class MenuBarGame extends JMenuBar {

	GameWindow window;
	Player player;
	public MenuBarGame(GameWindow window,Player player) {
		super();
		this.window=window;
		this.player=player;
		this.add(new MenuGame(window,player));
		this.add(new MenuOption(window));
	}
}
