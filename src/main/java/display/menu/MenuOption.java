package display.menu;

import display.game.GameWindow;

import javax.swing.*;

public class MenuOption extends JMenu{
	
	public MenuOption(GameWindow window) {
		super("option");
		this.add(new DisconnectItem(window));
	}
}
