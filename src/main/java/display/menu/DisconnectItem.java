package display.menu;

import display.connection.LoginWindow;
import display.game.GameWindow;
import service.RoundCalculatorService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class DisconnectItem extends JMenuItem implements ActionListener {
    GameWindow window;

    public DisconnectItem(GameWindow window) {
        super("Disconnect");
        this.window = window;
        this.addActionListener(this);
    }

    /**
     * Close current window by sending it a WINDOW_CLOSED event
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        RoundCalculatorService.getInstance().disable();
        window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSED));
        window.dispose();
        new LoginWindow();
    }

}
