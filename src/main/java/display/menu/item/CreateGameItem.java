package display.menu.item;

import bdd.DO.Player;
import display.game.CreateGamePanel;
import display.game.GameWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

public class CreateGameItem extends JMenuItem implements ActionListener{
	Logger LOGGER = Logger.getGlobal();
	JFrame window;
	Player creator;
	public CreateGameItem(Player creator, GameWindow window){
		super("create new game");
		this.window=window;
		this.addActionListener(this);
		this.creator = creator;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LOGGER.info("-- create a new game clicked --");
		//we change the component 
		window.setContentPane(new CreateGamePanel(creator));
		//referech the window
		SwingUtilities.updateComponentTreeUI(window);
		
	}
}
