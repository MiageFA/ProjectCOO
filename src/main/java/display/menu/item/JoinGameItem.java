package display.menu.item;

import bdd.DO.Player;
import display.game.GameWindow;
import display.game.JoinGamePanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JoinGameItem extends JMenuItem implements ActionListener{
	private Player player;
	private GameWindow window;
	public JoinGameItem(Player player,GameWindow window){
		super("join new game");
		this.player=player;
		this.window=window;
		this.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		window.setContentPane(new JoinGamePanel(player,window));
		SwingUtilities.updateComponentTreeUI(window);
		
	}
}
