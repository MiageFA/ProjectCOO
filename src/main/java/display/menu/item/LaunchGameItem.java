package display.menu.item;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import bdd.DO.Player;
import display.game.GameWindow;
import display.game.JoinGamePanel;
import display.game.LaunchGamePanel;

public class LaunchGameItem extends JMenuItem implements ActionListener{
	private Player player;
	private GameWindow window;
	public LaunchGameItem(Player player,GameWindow window){
		super("Launch game");
		this.player=player;
		this.window=window;
		this.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		window.setContentPane(new LaunchGamePanel(player,window));
		SwingUtilities.updateComponentTreeUI(window);
		
	}
}
