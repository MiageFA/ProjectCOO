package display;

import javax.swing.*;

public class Display extends JFrame {
    public Display(String title) {
        super();

        this.setTitle(title);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}