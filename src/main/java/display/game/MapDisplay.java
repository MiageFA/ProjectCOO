package display.game;

import bdd.DO.Coordinate;
import bdd.DO.GameMap;
import bdd.DO.Territory;
import display.footer.ActionDisplay;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class MapDisplay extends JPanel {
    private GameMap map;

    MapDisplay(GameMap gameMap, ActionDisplay actionDisplay) {
        super();
        this.map = gameMap;
        this.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        gridBagConstraints.anchor = GridBagConstraints.CENTER;
        gridBagConstraints.weightx = map.getWidth();
        gridBagConstraints.weighty = map.getHeight();

        for (Map.Entry<Coordinate, Territory> coordinateTerritoryEntry : map.getMap().entrySet()) {
            Coordinate coordinate = coordinateTerritoryEntry.getKey();
            Territory territory = coordinateTerritoryEntry.getValue();
            gridBagConstraints.gridx = coordinate.getX();
            gridBagConstraints.gridy =coordinate.getY() ;
//            if(coordinate.getY()==2 && coordinate.getY()==2){
//            	int a =2;
//            }
            add(new TerritoryDisplay(territory, actionDisplay), gridBagConstraints);
        }
    }
}
