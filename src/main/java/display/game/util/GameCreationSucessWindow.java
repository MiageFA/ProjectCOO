package display.game.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import display.Display;

public class GameCreationSucessWindow extends Display implements ActionListener{

	public GameCreationSucessWindow(String msg, Color color){
		super("Info");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(250, 200);
		
		JPanel container = new JPanel(new GridLayout(2, 1));
		JLabel msgLabel = new JLabel(msg);
		msgLabel.setForeground(color);
		container.add(msgLabel);
		
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(this);
		JPanel buttonContainer = new JPanel(new GridLayout(1, 3));
		buttonContainer.add(okButton,BorderLayout.CENTER);
		container.add(buttonContainer);
		
		this.add(container);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		
	}

}
