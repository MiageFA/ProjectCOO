package display.game;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Display Army
 */
public class ArmyDisplay extends JPanel {
	private BufferedImage image;

	ArmyDisplay(Integer nbArmies) {
		try {
			JPanel container = new JPanel();

			container.setLayout(new GridLayout(2, 10));

			JPanel head = new JPanel();
			JLabel label = new JLabel();
			label.setText(nbArmies.toString() + "");
			head.add(label, BorderLayout.WEST);

			container.add(head, BorderLayout.WEST);
			this.add(container);
            image = ImageIO.read(new File("./img/army.png"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Image toDraw  = image.getScaledInstance(getWidth(),getHeight(),Image.SCALE_SMOOTH);
        g.drawImage(toDraw, 0, 0, this);
    }
}
