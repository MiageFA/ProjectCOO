package display.game;

import bdd.DO.Player;
import display.game.listener.CreateGameButtonOKListener;

import javax.swing.*;
import java.awt.*;

public class CreateGamePanel extends JPanel{
	private JTextField namefield;
	private JTextField maxNumberPlayerField;
	private JTextField initNumberResourcePerPlayerField;
	private JTextField numberResourcePerRoundField;
	private JTextField initDistanceBetweenCitiesField;
	private JComboBox roundDurationTimeCombo;
	private JTextField maxArmiesPerTerritoryField;
	private JTextField heightMapField;
	private JTextField widthMapField;
	private Player creator;
	

	public CreateGamePanel(Player creator) {

		JLabel nameLabel = new JLabel("Nom: ");
		JLabel maxNumberPlayerLabel = new JLabel("Nombre de joueurs: ");
		JLabel initNumberResourcePerPlayerLabel = new JLabel("Nombre de ressources initiales:");
		JLabel numberResourcesPerRoundLabel = new JLabel("Nombre de ressources à distribuer à la fin de chaque tour");
		JLabel initDistanceBetweenCitiesLabel = new JLabel("Distance en les villes au départ de la partie");
		JLabel roundDurationTimePanelLabel = new JLabel("Durée pour un tour");
		JLabel maxArmiesPerTerritoryLabel = new JLabel("Nombre maximum d'armée dans un territoire");
		JLabel heighthMapLabel = new JLabel("Largeur de la carte");
		JLabel widthMapLabel = new JLabel("Longeur de la carte");

		JPanel namePanel = new JPanel();
		namePanel.setLayout(new GridLayout(1,2));
		
		JPanel maxNumberPlayersPanel = new JPanel();
		maxNumberPlayersPanel.setLayout(new GridLayout(1,2));
		
		JPanel initNumberResourcesPerPlayerPanel = new JPanel();
		initNumberResourcesPerPlayerPanel.setLayout(new GridLayout(1,2));
		
		JPanel numberResourcesPerRoundPanel = new JPanel();
		numberResourcesPerRoundPanel.setLayout(new GridLayout(1,2));
		
		JPanel initDistanceBetweenCitiesPanel = new JPanel();
		initDistanceBetweenCitiesPanel.setLayout(new GridLayout(1,2));
		
		JPanel roundDurationTimePanel = new JPanel();
		roundDurationTimePanel.setLayout(new GridLayout(1, 2));
		
		JPanel maxArmiesPerTerritoryPanel = new JPanel();
		maxArmiesPerTerritoryPanel.setLayout(new GridLayout(1,2));
		
		JPanel heightMapPanel = new JPanel();
		heightMapPanel.setLayout(new GridLayout(1, 2));
		
		JPanel widthtMapPanel = new JPanel();
		widthtMapPanel.setLayout(new GridLayout(1, 2));

		this.creator = creator;
		this.namefield = new JTextField("");
		this.maxNumberPlayerField = new JTextField("");
		this.initNumberResourcePerPlayerField = new JTextField("");
		this.numberResourcePerRoundField = new JTextField("");
		this.initDistanceBetweenCitiesField = new JTextField("");
		
		this.roundDurationTimeCombo = new JComboBox();
		this.roundDurationTimeCombo.addItem("1mn");
		this.roundDurationTimeCombo.addItem("2mn");
		this.roundDurationTimeCombo.addItem("3mn");
		this.roundDurationTimeCombo.addItem("5mn");
		this.roundDurationTimeCombo.addItem("10mn");
		this.roundDurationTimeCombo.addItem("20mn");
		this.roundDurationTimeCombo.addItem("1h");
		this.roundDurationTimeCombo.addItem("2h");
		this.roundDurationTimeCombo.addItem("3h");
		this.roundDurationTimeCombo.addItem("1j");
		
		this. maxArmiesPerTerritoryField = new JTextField("");
		this.widthMapField=new JTextField("");
		this.heightMapField=new JTextField("");
		
		namePanel.add(nameLabel);
		namePanel.add(namefield);
		
		maxNumberPlayersPanel.add(maxNumberPlayerLabel);
		maxNumberPlayersPanel.add(maxNumberPlayerField);
		
		initNumberResourcesPerPlayerPanel.add(initNumberResourcePerPlayerLabel);
		initNumberResourcesPerPlayerPanel.add(initNumberResourcePerPlayerField);
		
		numberResourcesPerRoundPanel.add(numberResourcesPerRoundLabel);
		numberResourcesPerRoundPanel.add(numberResourcePerRoundField);
		
		initDistanceBetweenCitiesPanel.add(initDistanceBetweenCitiesLabel);
		initDistanceBetweenCitiesPanel.add(initDistanceBetweenCitiesField);
		
		roundDurationTimePanel.add(roundDurationTimePanelLabel);
		roundDurationTimePanel.add(roundDurationTimeCombo);
		
		maxArmiesPerTerritoryPanel.add(maxArmiesPerTerritoryLabel);
		maxArmiesPerTerritoryPanel.add(maxArmiesPerTerritoryField);
		
		widthtMapPanel.add(widthMapLabel);
		widthtMapPanel.add(widthMapField);
		
		heightMapPanel.add(heighthMapLabel);
		heightMapPanel.add(heightMapField);
		
		this.setLayout(new GridLayout(10,1));
		this.add(namePanel);
		this.add(maxNumberPlayersPanel);
		this.add(initNumberResourcesPerPlayerPanel);
		this.add(numberResourcesPerRoundPanel);
		this.add(initDistanceBetweenCitiesPanel);
		this.add(roundDurationTimePanel);
		this.add(maxArmiesPerTerritoryPanel);
		this.add(heightMapPanel);
		this.add(widthtMapPanel);
		
		// ok button 
		JButton buttonOK = new JButton("OK");
		buttonOK.addActionListener(new CreateGameButtonOKListener(this, creator));
		this.add(buttonOK);
		
	}

	
	
	public JTextField getNamefield() {
		return namefield;
	}

	public void setNamefield(JTextField namefield) {
		this.namefield = namefield;
	}

	public JTextField getMaxNumberPlayerField() {
		return maxNumberPlayerField;
	}

	public void setMaxNumberPlayerField(JTextField maxNumberPlayerField) {
		this.maxNumberPlayerField = maxNumberPlayerField;
	}

	public JTextField getInitNumberResourcePerPlayerField() {
		return initNumberResourcePerPlayerField;
	}

	public void setInitNumberResourcePerPlayerField(JTextField initNumberResourcePerPlayerField) {
		this.initNumberResourcePerPlayerField = initNumberResourcePerPlayerField;
	}

	public JTextField getNumberResourcePerRoundField() {
		return numberResourcePerRoundField;
	}

	public void setNumberResourcePerRoundField(JTextField numberResourcePerRoundField) {
		this.numberResourcePerRoundField = numberResourcePerRoundField;
	}

	public JTextField getInitDistanceBetweenCitiesField() {
		return initDistanceBetweenCitiesField;
	}

	public void setInitDistanceBetweenCitiesField(JTextField initDistanceBetweenCitiesField) {
		this.initDistanceBetweenCitiesField = initDistanceBetweenCitiesField;
	}

	public JComboBox getRoundDurationTimeCombo() {
		return roundDurationTimeCombo;
	}

	public void setRoundDurationTimeCombo(JComboBox roundDurationTimeCombo) {
		this.roundDurationTimeCombo = roundDurationTimeCombo;
	}

	public JTextField getMaxArmiesPerTerritoryField() {
		return maxArmiesPerTerritoryField;
	}

	public void setMaxArmiesPerTerritoryField(JTextField maxArmiesPerTerritoryField) {
		this.maxArmiesPerTerritoryField = maxArmiesPerTerritoryField;
	}



	public JTextField getHeightMapField() {
		return heightMapField;
	}



	public void setHeightMapField(JTextField heightMapField) {
		this.heightMapField = heightMapField;
	}



	public JTextField getWidthMapField() {
		return widthMapField;
	}



	public void setWidthMapField(JTextField widthMapField) {
		this.widthMapField = widthMapField;
	}



}
