package display.game;

import bdd.DO.Game;
import bdd.DO.Player;
import display.Display;
import display.footer.ActionDisplay;
import display.menu.MenuBarGame;
import display.upper.PlayerInfos;
import service.RoundCalculatorService;

import javax.swing.*;
import java.awt.*;

public class GameWindow extends Display {
    private JPanel container;
    private MenuBarGame menuBar;
    private PlayerInfos playerInfos;

    public GameWindow(Game game, Player player){
        super("Game");
        setSize(900, 900);
        setLocationRelativeTo(null);

        container = new JPanel(new BorderLayout());

        menuBar = new MenuBarGame(this,player);
        menuBar.setLayout(new GridLayout(1, 5));
        setJMenuBar(menuBar);
        
        //la map
        //le footer (les actions)
        ActionDisplay actionDisplay = new ActionDisplay(game, player);
        actionDisplay.setPreferredSize(new Dimension(900, 60));
        //head les infos de player


        MapDisplay mapDisplay = new MapDisplay(game.getMap(), actionDisplay);
        
        playerInfos = new PlayerInfos(game, player);
        playerInfos.setPreferredSize(new Dimension(900, 40));
        
        container.add(playerInfos, BorderLayout.NORTH);
        container.add(mapDisplay);
        container.add(actionDisplay, BorderLayout.SOUTH);

        this.setContentPane(container);
        RoundCalculatorService.getInstance().setGame(game);
        System.out.println("*******"+RoundCalculatorService.getInstance().getState().name());
        if(! (RoundCalculatorService.getInstance().getState()== java.lang.Thread.State.RUNNABLE || RoundCalculatorService.getInstance().getState()==java.lang.Thread.State.TERMINATED)){
        	RoundCalculatorService.getInstance().start();
        }
        	
        setVisible(true);
    }
    
    public GameWindow(Player player){
        super("Game");
        setSize(900, 900);
        setLocationRelativeTo(null);

        menuBar = new MenuBarGame(this,player);
        menuBar.setLayout(new GridLayout(1, 5));
        setJMenuBar(menuBar);
        
        this.setContentPane(new JoinGamePanel(player,this));
        setVisible(true);
    }


}
