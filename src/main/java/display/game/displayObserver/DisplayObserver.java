package display.game.displayObserver;

/**
 *  Interface implemented by Display element to update their contents
 */
public interface DisplayObserver {
    void update();
}
