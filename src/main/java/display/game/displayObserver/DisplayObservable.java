package display.game.displayObserver;

public interface DisplayObservable {
    void addObserver(DisplayObserver displayObserver);

    void removeObserver(DisplayObserver displayObserver);

    void clearObservers();

    void notifyAllObservers();
}
