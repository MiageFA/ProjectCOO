package display.game.displayObserver;

import java.util.ArrayList;
import java.util.List;

public class DisplayObservableInstance implements DisplayObservable{
    private List<DisplayObserver> displayObservables;

    @Override
    public void clearObservers() {
        this.displayObservables.clear();
    }

    public DisplayObservableInstance()
    {
        displayObservables = new ArrayList<>();
    }

    @Override
    public void addObserver(DisplayObserver displayObserver) {
        this.displayObservables.add(displayObserver);
    }

    @Override
    public void removeObserver(DisplayObserver displayObserver) {
        this.displayObservables.remove(displayObserver);
    }

    @Override
    public void notifyAllObservers() {
        displayObservables.forEach(o -> o.update());
    }
}
