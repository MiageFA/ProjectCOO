package display.game;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import bdd.DO.Game;
import bdd.DO.Player;
import controller.form.GameController;
import display.game.listener.LaunchGameButtonListener;

public class LaunchGamePanel extends JPanel {
	private Player player;
	private JPanel globalContainer;
	JFrame window;
	GridBagConstraints gbc;

	public LaunchGamePanel(Player player, JFrame window) {
		this.window = window;
		this.player = player;
		this.globalContainer = new JPanel(new GridBagLayout());

		gbc = new GridBagConstraints();
		gbc.weightx = 1;
		gbc.weighty = 1;

		gbc.gridx = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.BOTH;

		JPanel head = new JPanel();
		JLabel titre = new JLabel("Launch a Game");
		head.add(titre);
		globalContainer.add(head, gbc);
		gbc.gridy = 1;
		globalContainer.add(new JLabel("games"), gbc);
		fillContainer();
		this.add(globalContainer);

	}

	public void refresh() {
		globalContainer.removeAll();
		gbc.weightx = 1;
		gbc.weighty = 1;

		gbc.gridx = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.BOTH;

		JPanel head = new JPanel();
		JLabel titre = new JLabel("Launch a Game");
		head.add(titre);
		globalContainer.add(head, gbc);
		gbc.gridy = 1;
		globalContainer.add(new JLabel("games"), gbc);
		fillContainer();
		SwingUtilities.updateComponentTreeUI(window);
	}

	private void fillContainer() {

		List<Game> games = GameController.getInstance().getGamesToLaunch(player);
		System.out.println(games.size());
		for (Game game : games) {
			JPanel container = new JPanel(new GridLayout(1, 2));
			JLabel gameLabel = new JLabel(game.toString());
			JButton joinButton = new JButton("Launch");
			joinButton.addActionListener(new LaunchGameButtonListener(game, player, this));
			container.add(gameLabel);
			container.add(joinButton);
			gbc.gridy = gbc.gridy + 1;
			globalContainer.add(container, gbc);
		}

	}
}
