package display.game;

import bdd.DO.Player;
import bdd.DO.Territory;
import display.footer.ActionDisplay;
import display.game.listener.TerritoryListener;
import model.ADomainObject;
import util.Observer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  Display a territory
 *  Is an observer of Territory element to be updated when element change
 */
public class TerritoryDisplay extends JPanel implements Observer {
    private Territory territory;
    Map<Player, Long> nbArmiesByPlayers;
    private BufferedImage image;
    List<JPanel> jPanelList;

    public TerritoryDisplay(Territory territory, ActionDisplay actionDisplay) {
        super();
        jPanelList = new ArrayList<>();
        try {
            this.territory = territory;
            territory.addObserver(this);
            switch (territory.getTerritoryType()) {
                case MOUNTAIN:
                    image = ImageIO.read(new File("./img/mountain-cartoon.png"));
                    break;
                case PLAIN:
                    image = ImageIO.read(new File("./img/plain-cartoon.png"));
                    break;
                case FIELD:
                    image = ImageIO.read(new File("./img/field-cartoon.png"));
                    break;
            }
            setArmies();
            setCity();
            addMouseListener(new TerritoryListener(territory, actionDisplay.getCreateCity(),
                    actionDisplay.getDestroyCity(),
                    actionDisplay.getCreateArmy(),
                    actionDisplay.getMoveArmy()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCity()
    {
        if (territory.getCity() != null)
        {
            CityDisplay cityDisplay = new CityDisplay();
            add(cityDisplay, BorderLayout.WEST);
            jPanelList.add(cityDisplay);
        }
    }

    public void setArmies() {
        nbArmiesByPlayers = territory.getNbArmiesByPlayer();
        for (Map.Entry<Player, Long> armiesByPlayer : nbArmiesByPlayers.entrySet()) {
            if (armiesByPlayer.getValue() > 0);
            {
                ArmyDisplay armyDisplay = new ArmyDisplay(armiesByPlayer.getValue().intValue());
                add(armyDisplay);
                jPanelList.add(armyDisplay);
            }
        }
    }

    public void update(ADomainObject aDomainObject) {
        for (JPanel jPanel : jPanelList)
            remove(jPanel);
        setArmies();
        setCity();
        SwingUtilities.updateComponentTreeUI(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Image toDraw  = image.getScaledInstance(getWidth(),getHeight(),Image.SCALE_SMOOTH);
        g.drawImage(toDraw, 0, 0, this);
    }
}
