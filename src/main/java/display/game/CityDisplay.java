package display.game;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CityDisplay extends JPanel {
	private BufferedImage image;

	CityDisplay() {
		try {
			JPanel container = new JPanel();

			this.add(container);
            image = ImageIO.read(new File("./img/city.png"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Image toDraw  = image.getScaledInstance(getWidth(),getHeight(),Image.SCALE_SMOOTH);
        g.drawImage(toDraw, 0, 0, this);
    }
}
