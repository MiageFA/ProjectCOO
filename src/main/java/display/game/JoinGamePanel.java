package display.game;

import bdd.DO.Game;
import bdd.DO.Player;
import controller.form.GameController;
import display.game.listener.JoinButtonListener;
import util.State;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class JoinGamePanel extends JPanel {
    private Player player;
    private JPanel globalContainer;
    JFrame window;
    GridBagConstraints gbc;

    public JoinGamePanel(Player player, JFrame window) {
        this.window = window;
        this.player = player;
        this.globalContainer = new JPanel(new GridBagLayout());

        gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 1;

        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;

        JPanel head = new JPanel();
        JLabel titre = new JLabel("Join a Game");
        head.add(titre);
        globalContainer.add(head, gbc);
        gbc.gridy = 1;
        globalContainer.add(new JLabel("games"), gbc);
        fillContainer();
        this.add(globalContainer);

    }

    public void refresh() {
        globalContainer.removeAll();
        gbc.weightx = 1;
        gbc.weighty = 1;

        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;

        JPanel head = new JPanel();
        JLabel titre = new JLabel("Join a Game");
        head.add(titre);
        globalContainer.add(head, gbc);
        gbc.gridy = 1;
        globalContainer.add(new JLabel("the games"), gbc);
        fillContainer();
        SwingUtilities.updateComponentTreeUI(window);
    }

    private void fillContainer() {

        List<Game> games = GameController.getInstance().getGamesWaiting(player, State.WAITING);
        for (Game game : games) {
            JPanel container = new JPanel(new GridLayout(1, 2));
            JLabel gameLabel = new JLabel(game.toString());
            JButton joinButton = new JButton("join");
            joinButton.addActionListener(new JoinButtonListener(game, player, this));
            container.add(gameLabel);
            container.add(joinButton);
            gbc.gridy = gbc.gridy + 1;
            globalContainer.add(container, gbc);
        }

    }
}
