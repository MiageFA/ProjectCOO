package display.game.listener;

import bdd.DO.Territory;
import display.game.displayObserver.DisplayObservableInstance;

public class GlobalListenerService extends DisplayObservableInstance {
    private static GlobalListenerService globalListenerService;
    private Territory departure;
    private Territory destination = null;
    private boolean moveArmy = false;
    private boolean isReady = false;
    private boolean actionComplete = false;

    public GlobalListenerService()
    {
        super();
    }

    public static GlobalListenerService getInstance() {
        if (globalListenerService == null)
            globalListenerService = new GlobalListenerService();
        return globalListenerService;
    }

    public Territory getDeparture() {
        return departure;
    }

    public void setDeparture(Territory departure) {
        clear();
        this.departure = departure;
    }

    public Territory getDestination() {
        return destination;
    }

    public void setDestination(Territory destination) {
        this.destination = destination;
        this.isReady = true;
    }

    public void setMoveArmy(boolean moveArmy) {
        this.moveArmy = moveArmy;
    }

    public boolean isMoveArmy() {
        return moveArmy;
    }

    public boolean isReady() {
        return isReady;
    }

    /***TODO: uncomment all of that*/
    public void setActionComplete(boolean actionComplete) {
/*        this.actionComplete = actionComplete;

        if (this.actionComplete)
            notifyAllObservers();*/
    }

    public boolean isActionComplete() {
        return actionComplete;
    }

    public void clear()
    {
        destination = null;
        departure = null;
        moveArmy = false;
        isReady = false;
    }
}
