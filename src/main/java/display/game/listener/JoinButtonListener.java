package display.game.listener;

import bdd.DO.Game;
import bdd.DO.Player;
import display.game.JoinGamePanel;
import service.gameService.GameService;
import util.UnitOfWork.UnitOfWork;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JoinButtonListener implements ActionListener{
	private Game game;
	private Player player;
	private JoinGamePanel panel;
	public JoinButtonListener(Game game,Player player,JoinGamePanel panel){
		this.game=game;
		this.player=player;
		this.panel=panel;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		GameService.getInstance().joinGame(player, game);
		panel.refresh();
		
	}

}
