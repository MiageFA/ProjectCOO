package display.game.listener;

import bdd.DO.Territory;
import display.footer.ActionButton;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class TerritoryListener implements MouseListener {
    private Territory territory;
    private ActionButton createCity;
    private ActionButton destroyCity;
    private ActionButton createArmy;
    private ActionButton moveArmy;
    private GlobalListenerService globalListenerService;


    public TerritoryListener(Territory territory,
                             ActionButton createCity,
                             ActionButton destroyCity,
                             ActionButton createArmy,
                             ActionButton moveArmy)
    {
        this.territory = territory;
        this.createCity = createCity;
        this.destroyCity = destroyCity;
        this.createArmy = createArmy;
        this.moveArmy = moveArmy;
        this.globalListenerService = GlobalListenerService.getInstance();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    public void setButtonsTerritory()
    {
        createCity.getActionButtonListener().setCoordinate(this.territory.getCoordinate());
        destroyCity.getActionButtonListener().setCoordinate(this.territory.getCoordinate());
        createArmy.getActionButtonListener().setCoordinate(this.territory.getCoordinate());
        moveArmy.getActionButtonListener().setCoordinate(this.territory.getCoordinate());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (globalListenerService.isMoveArmy()) {
            globalListenerService.setDestination(territory);
        }
        else
            globalListenerService.setDeparture(territory);
        createCity.setVisibility(territory.getCoordinate(), null);
        destroyCity.setVisibility(territory.getCoordinate(), null);
        createArmy.setVisibility(territory.getCoordinate(), null);
        moveArmy.setVisibility(territory.getCoordinate(), globalListenerService.getDestination());
        setButtonsTerritory();
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
