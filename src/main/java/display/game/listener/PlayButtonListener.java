package display.game.listener;

import bdd.DO.Game;
import bdd.DO.Player;
import display.game.GameWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayButtonListener implements ActionListener {
    private Game game;
    private Player player;
    private GameWindow window;

    public PlayButtonListener(Game game, Player player, GameWindow window) {
        this.game = game;
        this.player = player;
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
    	window.dispose();
        new GameWindow(game, player);
        SwingUtilities.updateComponentTreeUI(window);
    }
}
