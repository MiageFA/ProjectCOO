package display.game.listener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bdd.DO.Player;
import controller.form.GameController;
import controller.form.GameForm;
import display.game.CreateGamePanel;
import display.game.util.GameCreationSucessWindow;

public class CreateGameButtonOKListener implements ActionListener {
	private CreateGamePanel panel;
	
	private final String CREATION_SUCESS="Game Creation Sucess";
	private final String CREATION_FAILED="Game Creation failed";
	private Player creator;

	public CreateGameButtonOKListener(CreateGamePanel panel, Player creator) {
		this.panel = panel;
		this.creator = creator;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {

			GameForm gameForm = getGameFrom();
			GameController.getInstance().createGame(gameForm, creator);
			clearField();
			new GameCreationSucessWindow(CREATION_SUCESS,Color.blue);
		} catch (Exception exp) {
			exp.printStackTrace();
			new GameCreationSucessWindow(CREATION_FAILED,Color.red);
		}

	}
	
	private void clearField(){
		panel.getNamefield().setText("");
		panel.getMaxArmiesPerTerritoryField().setText("");
		panel.getNumberResourcePerRoundField().setText("");
		panel.getInitNumberResourcePerPlayerField().setText("");
		panel.getNumberResourcePerRoundField().setText("");
		panel.getInitDistanceBetweenCitiesField().setText("");
		panel.getMaxArmiesPerTerritoryField().setText("");
		panel.getWidthMapField().setText("");
		panel.getHeightMapField().setText("");
	}
	private GameForm getGameFrom(){
		GameForm gameForm = null;
		try{
			
		String name = panel.getNamefield().getText();
		Integer maxNumberPlayers = Integer.parseInt(panel.getMaxNumberPlayerField().getText());
		Integer initNumberResourcesPerPlayer = Integer
				.parseInt(panel.getInitNumberResourcePerPlayerField().getText());
		Integer numberResourcesPerRound = Integer.parseInt(panel.getNumberResourcePerRoundField().getText());
		Integer initDistanceBetweenCities = Integer.parseInt(panel.getInitDistanceBetweenCitiesField().getText());
		String str = panel.getRoundDurationTimeCombo().getSelectedItem().toString();
		Integer roundDurationTime = getSeconds(str);
		Integer maxArmiesPerTerritories = Integer.parseInt(panel.getMaxArmiesPerTerritoryField().getText());
		Integer widthMap = Integer.parseInt(panel.getWidthMapField().getText());
		Integer heightMap = Integer.parseInt(panel.getHeightMapField().getText());

		gameForm = new GameForm();
		gameForm.setName(name);
		gameForm.setMaxNumberPlayers(maxNumberPlayers);
		gameForm.setInitNumberResourcesPerPlayer(initNumberResourcesPerPlayer);
		gameForm.setNumberResourcesPerRound(numberResourcesPerRound);
		gameForm.setInitDistanceBetweenCities(initDistanceBetweenCities);
		gameForm.setRoundDurationTime(roundDurationTime);
		gameForm.setMaxArmiesPerTerritories(maxArmiesPerTerritories);
		gameForm.setHeightMap(heightMap);
		gameForm.setWidthMap(widthMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return gameForm;
	}

	private Integer getSeconds(final String RoundDuration) {
		Integer seconds = 0;
		if (RoundDuration.endsWith("mn")) {
			Integer mn = Integer.parseInt(RoundDuration.substring(0, RoundDuration.indexOf("mn")));
			seconds = mn * 60;
		} else if (RoundDuration.endsWith("h")) {
			Integer h = Integer.parseInt(RoundDuration.substring(0, RoundDuration.indexOf("h")));
			seconds = h * 60 * 60;
		} else if (RoundDuration.endsWith("j")) {
			Integer d = Integer.parseInt(RoundDuration.substring(0, RoundDuration.indexOf("j")));
			seconds = d * 24 * 60 * 60;
		}

		return seconds;
	}
	
	

}
