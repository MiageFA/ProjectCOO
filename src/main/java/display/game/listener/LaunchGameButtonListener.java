package display.game.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bdd.DO.Game;
import bdd.DO.Player;
import display.game.JoinGamePanel;
import display.game.LaunchGamePanel;
import service.gameService.GameService;
import util.UnitOfWork.UnitOfWork;

public class LaunchGameButtonListener implements ActionListener{
	private Game game;
	private Player player;
	private LaunchGamePanel panel;
	public LaunchGameButtonListener(Game game,Player player,LaunchGamePanel panel){
		this.game=game;
		this.player=player;
		this.panel=panel;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		GameService.getInstance().launchGame(game);
		panel.refresh();
		
	}
}
