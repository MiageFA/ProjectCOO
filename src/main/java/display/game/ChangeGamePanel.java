package display.game;

import bdd.DO.Game;
import bdd.DO.Player;
import controller.form.GameController;
import display.game.listener.PlayButtonListener;
import util.State;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ChangeGamePanel extends JPanel {
    private Player player;
    private JPanel globalContainer;
    GameWindow window;
    GridBagConstraints gbc;

    public ChangeGamePanel(Player player, GameWindow window) {
        this.window = window;
        this.player = player;
        this.globalContainer = new JPanel(new GridBagLayout());

        gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 1;

        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;

        JPanel head = new JPanel();
        JLabel titre = new JLabel("Change game");
        head.add(titre);
        globalContainer.add(head, gbc);
        gbc.gridy = 1;
        globalContainer.add(new JLabel("games"), gbc);
        fillContainer();
        this.add(globalContainer);
    }

    public void refresh() {
        globalContainer.removeAll();
        gbc.weightx = 1;
        gbc.weighty = 1;

        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;

        JPanel head = new JPanel();
        JLabel titre = new JLabel("Play a Game");
        head.add(titre);
        globalContainer.add(head, gbc);
        gbc.gridy = 1;
        globalContainer.add(new JLabel("games"), gbc);
        fillContainer();
        SwingUtilities.updateComponentTreeUI(window);
    }

    private void fillContainer() {
        List<Game> games = GameController.getInstance().getGamesWaiting(player, State.IN_PROGRESS);
        for (Game game : games) {
            JPanel container = new JPanel(new GridLayout(1, 2));
            JLabel gameLabel = new JLabel(game.toString());
            JButton playButton = new JButton("Play");
            playButton.addActionListener(new PlayButtonListener(game, player, window));
            container.add(gameLabel);
            container.add(playButton);
            gbc.gridy = gbc.gridy + 1;
            globalContainer.add(container, gbc);
        }
    }
}
