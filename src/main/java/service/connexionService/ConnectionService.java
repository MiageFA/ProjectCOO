package service.connexionService;


import bdd.DO.Player;
import bdd.dao.mapper.PlayerMapper;

public class ConnectionService {
    public static ConnectionService connexionService;

    ConnectionService() {
    }

    public static ConnectionService getConnexionService() {
        if (connexionService == null)
            connexionService = new ConnectionService();
        return connexionService;
    }

    public static Player connect(String pseudo) {
        PlayerMapper playerMapper=PlayerMapper.getInstance();
        return playerMapper.findByLogin(pseudo);
        
    }
}
