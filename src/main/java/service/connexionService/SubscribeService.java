package service.connexionService;

import bdd.DO.Player;
import bdd.dao.mapper.PlayerMapper;
import controller.form.NewPlayerForm;

public class SubscribeService {
    static SubscribeService subscribeService;
    private PlayerMapper playerMapper;

    SubscribeService() {
        playerMapper = PlayerMapper.getInstance();
    }

    public static SubscribeService getSubscribeService() {
        if (subscribeService == null)
            subscribeService = new SubscribeService();
        return subscribeService;
    }

    public Player subscribe(NewPlayerForm newPlayerForm) {
        Player player = new Player();
        player.setFirstName(newPlayerForm.getFirstName());
        player.setLastName(newPlayerForm.getLastName());
        player.setPassword(newPlayerForm.getPassword());
        player.setPseudo(newPlayerForm.getNickName());
        Integer id = playerMapper.insert(player);
        if (id == null)
            return null;
        player.setId(id);
        return player;
    }
}
