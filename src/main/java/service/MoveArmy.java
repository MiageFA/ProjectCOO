package service;

import bdd.DO.*;
import display.game.listener.GlobalListenerService;
import util.CalculatePaths;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MoveArmy implements Action {
    GlobalListenerService globalListenerService;

    final int price = 1;

    private Integer getPathPrice(Game game, List<Coordinate> coordinates) {
        Integer pathPrice = 0;

        coordinates.remove(0);
        for (Coordinate coordinate : coordinates) {
            TerritoryTypeEnum territoryTypeEnum = game.getMap().getTerritory(coordinate).getType();
            pathPrice += ((territoryTypeEnum == TerritoryTypeEnum.MOUNTAIN)) ? (price * 2) : (price);
        }
        return pathPrice;
    }

    private int computePrice(Game game, Coordinate departure, Territory destination) {
        Integer totalPrice;
        CalculatePaths calculatePaths = new CalculatePaths();
        List<List<Coordinate>> possiblePaths = calculatePaths.calculatePaths(departure, destination.getCoordinate());
        List<Integer> prices = new ArrayList<>();

        for (List<Coordinate> coordinates : possiblePaths)
            prices.add(getPathPrice(game, coordinates));
        totalPrice = prices.stream().min(Comparator.comparingInt(Integer::intValue)).get();
        return totalPrice;
    }

    private boolean playerHaveAnArmyOnTerritory(Territory territory, Player player) {
        return territory.getArmies().stream().filter(a -> a.getPlayer().equals(player)).findFirst().isPresent();
    }

    private Army getArmyOnTerritory(Territory territory, Player player) {
        return territory.getArmies().stream().filter(a -> a.getPlayer().equals(player)).findFirst().get();
    }

    private boolean verifyDestination(Territory destination) {
        return (destination.getArmies().size() < destination.getMaxNumberArmies());
    }


    /**
     * Verify if player can execute this action
     * @param game
     * @param coordinate
     * @param player
     * @param param
     * @return
     */
    @Override
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        globalListenerService = GlobalListenerService.getInstance();
        Territory destination = globalListenerService.getDestination();
        Territory departure = globalListenerService.getDeparture();
        if (globalListenerService.getDeparture() != null && globalListenerService.getDestination() == null && playerHaveAnArmyOnTerritory(departure, player)) {
            return true;
        }
        if (destination == null || !playerHaveAnArmyOnTerritory(departure, player)
                || !verifyDestination(game.getMap().getTerritory(destination.getCoordinate()))
                || game.getCurrentRound().getPlayers().contains(player))
            return false;
        return cost(game, departure.getCoordinate(), player, destination);
    }

    @Override
    public boolean cost(Game game, Coordinate coordinate, Player player, Object param) {
        Territory destination = (Territory) param;
        return (player.canPay(computePrice(game, coordinate, destination), game));
    }

    /***
     * Look if player can move an army, then, compute the price, delete an army from departure territory, add one on destination and update army's coordinate
     *
     * @param game : current game
     * @param coordinate : departure coordinate
     * @param player : current player
     * @param param : destination coordinate
     * @return
     */
    @Override
    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        globalListenerService = GlobalListenerService.getInstance();
        if (!globalListenerService.isMoveArmy()) {
            globalListenerService.setDeparture(game.getMap().getTerritory(coordinate));
            globalListenerService.setMoveArmy(true);
        }
        if (!canExecute(game, coordinate, player, param) || !globalListenerService.isReady())
            return false;
        Territory departure = globalListenerService.getDeparture();
        Territory destination = globalListenerService.getDestination();
        Army army = getArmyOnTerritory(departure, player);
        destination.addArmy(army);
        departure.removeArmy(army);
        army.setTerritory(destination);
        player.pay(computePrice(game, globalListenerService.getDeparture().getCoordinate(), destination), game);
        destination.reevaluateController();
        departure.reevaluateController();
        globalListenerService.clear();
        globalListenerService.setActionComplete(true);
        game.getCurrentRound().addPlayer(player);
        return true;
    }
}
