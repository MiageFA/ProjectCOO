package service;

import bdd.DO.*;
import display.game.listener.GlobalListenerService;

public class CreateCity implements Action {
    final int price = 5;
    private GlobalListenerService globalListenerService = GlobalListenerService.getInstance();

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        Territory territory = game.getMap().getTerritory(coordinate);
        if (!player.equals(territory.reevaluateController()) || territory.getCity() != null
        || game.getCurrentRound().getPlayers().contains(player))
            return false;
        return (player.canPay(price, game));
    }

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean cost(Game game, Coordinate coordinate, Player player, Object param) {
        return (player.canPay(price, game));
    }

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        if (!canExecute(game, coordinate, player, param))
            return false;
        City city = new City(player);
        player.getCityList().add(city);
        player.pay(price, game);
        globalListenerService.setActionComplete(true);
        game.getCurrentRound().addPlayer(player);
        return game.getMap().getTerritory(coordinate).setCity(city, player);
    }
}
