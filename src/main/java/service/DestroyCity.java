package service;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import bdd.DO.Territory;
import display.game.listener.GlobalListenerService;

public class DestroyCity implements Action {
    final int price = 4;
    private GlobalListenerService globalListenerService = GlobalListenerService.getInstance();

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        Territory territory = game.getMap().getTerritory(coordinate);
        int playerArmiesCount, controllerArmiesCount;
        playerArmiesCount = territory.countArmyByPlayer(player);
        controllerArmiesCount = territory.countArmyByPlayer(territory.getController());
        if (territory.getCity() == null
                || playerArmiesCount == 0
                || player.equals(territory.getController())
                || game.getCurrentRound().getPlayers().contains(player)) {
            System.out.println("City == " + territory.getCity());
            System.out.println("ArmiesCount "+ playerArmiesCount);
            System.out.println("Controller != player " + player.equals(territory.getController()));
            return false;
        }
        playerArmiesCount -= controllerArmiesCount;
        System.out.println("++ d'armées : " + playerArmiesCount);
        if (!(playerArmiesCount >= controllerArmiesCount))
            return false;
        return cost(game, coordinate, player, param);
    }

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean cost(Game game, Coordinate coordinate, Player player, Object param) {
        return (player.canPay(price, game));
    }

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        if (!canExecute(game, coordinate, player, param))
            return false;
        Territory territory = game.getMap().getTerritory(coordinate);
        territory.destroyCity();
        player.pay(price, game);
        globalListenerService.setActionComplete(true);
        game.getCurrentRound().addPlayer(player);
        return true;
    }
}
