package service;

import bdd.DO.*;
import display.game.listener.GlobalListenerService;

public class CreateArmy implements Action {
    int price = 3;
    private GlobalListenerService globalListenerService = GlobalListenerService.getInstance();

    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        Territory territory = game.getMap().getTerritory(coordinate);
        if (territory == null || (territory.getArmies().size() >= territory.getMaxNumberArmies())
                || game.getCurrentRound().getPlayers().contains(player))
            return false;
        return player.canPay(price, game);
    }


    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean cost(Game game, Coordinate coordinate, Player player, Object param) {
        return player.canPay(price, game);
    }


    /**
     *
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    @Override
    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        if (!canExecute(game, coordinate, player, param))
            return false;
        Territory territory = game.getMap().getTerritory(coordinate);
        Army army = new Army(player);
        if (!(player.addArmy(army) && territory.addArmy(army)))
            return false;
        player.pay(price, game);
        army.setTerritory(territory);
        globalListenerService.setActionComplete(true);
        game.getCurrentRound().addPlayer(player);
        return (true);
    }
}