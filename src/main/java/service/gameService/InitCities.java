package service.gameService;

import bdd.DO.City;
import bdd.DO.Coordinate;
import bdd.DO.GameMap;
import bdd.DO.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InitCities {
    private boolean distanceBetweenTerritoriesIsLongEnought(int x, int y, Coordinate coordinate, Integer minDistance) {
        return (Math.abs(x - coordinate.getX()) >= minDistance
                || Math.abs(y - coordinate.getY()) >= minDistance
                || (Math.abs(x - coordinate.getX()) + Math.abs(y - coordinate.getY()) >= minDistance));
    }

    private void addCity(GameMap gameMap, Player player, Coordinate coordinate) {
        City city = new City(player);
        player.addCity(city);
        gameMap.getTerritory(coordinate).setCity(city, player);
    }

    private Coordinate initLoopCoordinate(List<Coordinate> coordinates)
    {
        Iterator<Coordinate> it = coordinates.iterator();
        Coordinate coordinate = null;
        while (it.hasNext())
            coordinate = it.next();
        return coordinate;
    }

    /**
     * Compute city position on map
     * @param gameMap : current gameMap
     * @param players : List of players in game
     * @param minDistance : minimal distance between cities
     */
    public void initCities(GameMap gameMap, List<Player> players, Integer minDistance) {

        List<Coordinate> coordinateList = new ArrayList<>();
        for (Player p : players) {
            boolean creation = false;
            while (!creation) {
                for (Integer x = 0; x < gameMap.getWidth(); x++) {
                    for (Integer y = 0; y < gameMap.getHeight(); y++) {
                        if (x == 0 && y == 0 && !coordinateList.isEmpty()) {
                            Coordinate coordinate = initLoopCoordinate(coordinateList);
                            x = coordinate.getX();
                            y = coordinate.getY();
                        }
                        creation = (x == 0 && y == 0 && coordinateList.isEmpty());
                        if (!creation)
                            for (Coordinate coordinate : coordinateList) {
                                creation = distanceBetweenTerritoriesIsLongEnought(x, y, coordinate, minDistance);
                            }
                        if (creation) {
                            Coordinate coordinate = new Coordinate(x, y);
                            addCity(gameMap, p, coordinate);
                            coordinateList.add(coordinate);
                            break;
                        }
                    }
                    if (creation)
                        break;
                }
            }
        }
    }
}
