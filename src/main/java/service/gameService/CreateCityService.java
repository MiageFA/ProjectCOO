package service.gameService;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import controller.display.ActionController;
import service.CreateCity;

public class CreateCityService{
    private static CreateCityService createCityService = null;

    public static CreateCityService getInstance() {
        if (createCityService == null)
            createCityService = new CreateCityService();
        return createCityService;
    }
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateCity createCity = new CreateCity();

        return createCity.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateCity createCity = new CreateCity();

        return createCity.execute(game, coordinate, player, param);
    }
}
