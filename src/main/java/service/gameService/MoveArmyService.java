package service.gameService;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import controller.display.ActionController;
import service.MoveArmy;

public class MoveArmyService{
    private static MoveArmyService moveArmyService = null;

    public static MoveArmyService getInstance() {
        if (moveArmyService == null)
            moveArmyService = new MoveArmyService();
        return moveArmyService;
    }
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        MoveArmy moveArmy = new MoveArmy();

        return moveArmy.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        MoveArmy moveArmy = new MoveArmy();

        return moveArmy.execute(game, coordinate, player, param);
    }
}
