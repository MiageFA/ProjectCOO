package service.gameService;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.DestroyCity;

public class DestroyCityService {
    private static DestroyCityService destroyCityService = null;

    public static DestroyCityService getInstance() {
        if (destroyCityService == null)
            destroyCityService = new DestroyCityService();
        return destroyCityService;
    }
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        DestroyCity destroyCity = new DestroyCity();

        return destroyCity.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        DestroyCity destroyCity = new DestroyCity();

        return destroyCity.execute(game, coordinate, player, param);
    }
}
