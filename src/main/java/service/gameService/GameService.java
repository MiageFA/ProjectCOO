package service.gameService;

import bdd.DO.Game;
import bdd.DO.Player;
import bdd.dao.mapper.GameMapper;
import service.RoundCalculatorService;
import util.State;
import util.UnitOfWork.UnitOfWork;

import java.util.ArrayList;
import java.util.List;

public class GameService {
    private static GameService gameService = null;
    private UnitOfWork unitOfWork = null;
    private GameMapper gameMapper = null;

    public GameService() {
        unitOfWork = UnitOfWork.getInstance();
        gameMapper = GameMapper.getInstance();
    }

    public static GameService getInstance() {
        return gameService = (gameService == null) ? (new GameService()) : gameService;
    }

    public void cancel() {
        unitOfWork.clear();
    }

    public void commit() {
        unitOfWork.commit();
    }

    public Game createGame(Integer maxPlayers, Integer maxRounds, Integer initialRessources, Integer ressourcesPerRound,
                           Integer roundTime, String name, Player player, Integer width, Integer height, Integer maxArmiesPerTerritory) {

        Game game;

        game = new Game(maxPlayers, maxRounds, initialRessources, ressourcesPerRound, roundTime, name, player, width, height, maxArmiesPerTerritory, player);
        game.setId(gameMapper.insert(game));
        return game;
    }

    public Game createGame(Game game, Player creator) {
        game.setCreator(creator);
        game.setState(State.WAITING);
        game.setId(gameMapper.insert(game));
        game.addPlayer(creator);
        gameMapper.update(game);
        return game;
    }

    public Game getGameById(Integer id) {
        return gameMapper.findByID(id);
    }

    public List<Game> getGamesByCreator(Player player) {
        return gameMapper.getGamesByCreator(player);
    }

    public void joinGame(Player player, Game game) {
        game.addPlayer(player);
        UnitOfWork.getInstance().commit();
    }

    /**
     * Launch the game in parameter
     * @param game
     */
    public void launchGame(Game game) {
        game.setState(State.IN_PROGRESS);
        game.initCities();
        UnitOfWork.getInstance().commit();
    }

    /**
     * Return game ready to get launched
     * @param player
     * @return
     */
    public List<Game> getGamesToLaunch(Player player) {
        List<Game> gamesCreated = gameMapper.getGamesByCreator(player);
        List<Game> gamesToLaunch = new ArrayList();
        for (Game game : gamesCreated) {
            if (game.getPlayers().size() == game.getMaxNumberPlayers() && State.WAITING.equals(game.getState())) {
                gamesToLaunch.add(game);
            }
        }
        return gamesToLaunch;
    }

    /**
     * Get games the player participate to, depending of their state
     * @param p : player
     * @param state : game state
     * @return
     */
    public List<Game> getGamesByState(Player p, State state) {
        List<Game> gameList = GameMapper.getInstance().getGamesByState(p, state);
        return gameList;
    }

}
