package service.gameService;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.CreateArmy;

public class CreateArmyService {
    private static CreateArmyService createArmyService = null;

    public static CreateArmyService getInstance() {
        if (createArmyService == null)
            createArmyService = new CreateArmyService();
        return createArmyService;
    }
    
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateArmy createArmy = new CreateArmy();

        return createArmy.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateArmy createArmy = new CreateArmy();

        return createArmy.execute(game, coordinate, player, param);
    }
}
