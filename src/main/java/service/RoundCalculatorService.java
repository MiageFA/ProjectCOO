package service;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import bdd.DO.Game;
import bdd.DO.Round;
import util.UnitOfWork.UnitOfWork;

public class RoundCalculatorService extends Thread {
	Logger LOGGER = Logger.getGlobal();
	private final static RoundCalculatorService INSTANCE = new RoundCalculatorService();
	private Game game;
	private Boolean stateService;

	private RoundCalculatorService() {

	}

    public static RoundCalculatorService getInstance() {
        return INSTANCE;
    }

	public void activate() {
		LOGGER.info("Round Calculator service is running");
		this.stateService = true;
		if (game != null) {

			while (stateService) {
				if (game.getCurrentRound() == null) {
					Round round = new Round();
					Date dateBegin = Calendar.getInstance().getTime();
					LOGGER.info(dateBegin.toString());
					round.setDateBegin(dateBegin);
					round.setDateEnd(new Date(dateBegin.getTime() + game.getRoundDurationTime() * 1000));
					round.setGame(game);
					round.setRoundNumber(1);
					game.setCurrentRound(round);
				} else if (game.getCurrentRound().getDateEnd().before(Calendar.getInstance().getTime())) {
					Round round = new Round();
					Date dateBegin = Calendar.getInstance().getTime();
					LOGGER.info(dateBegin.toString());
					round.setDateBegin(dateBegin);
					round.setDateEnd(new Date(dateBegin.getTime() + game.getRoundDurationTime() * 1000));
					round.setGame(game);
					round.setRoundNumber(game.getCurrentRound().getRoundNumber() + 1);
					game.updateCurrentRound(round);
				}
			}
		}
	}

    public void run() {
        this.activate();
    }

	public void disable() {
		this.stateService = false;
	}


	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}


	public void setStateService(Boolean state) {
		this.stateService = state;
	}

	public Boolean getStateService() {
		return stateService;
	}

	public static void main(String[] args) {
		UnitOfWork ut = UnitOfWork.getInstance();
		Game game = new Game();
		game.setRoundDurationTime(5);
		game.setName("game1");
		RoundCalculatorService calculator = RoundCalculatorService.getInstance();
		calculator.setGame(game);
		calculator.start();

	}

}
