package service;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;

public interface Action {

    /**
     *  execute an action
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    boolean execute(Game game, Coordinate coordinate, Player player, Object param);

    /**
     *  Verify if player can execute this action
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    boolean canExecute(Game game, Coordinate coordinate, Player player, Object param);

    /**
     *  Compute price of the action
     * @param game : current game
     * @param coordinate : current coordinate
     * @param player : current player
     * @param param : aditionnal parameters
     * @return
     */
    boolean cost(Game game, Coordinate coordinate, Player player, Object param);
}
