package controller.display;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.gameService.CreateArmyService;

public class CreateArmyController implements ActionController {
    private static CreateArmyController createArmyController = null;

    public static CreateArmyController getInstance() {
         if (createArmyController == null)
             createArmyController = new CreateArmyController();
         return createArmyController;
    }

    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateArmyService createArmy = CreateArmyService.getInstance();

        return createArmy.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateArmyService createArmy = CreateArmyService.getInstance();

        return createArmy.execute(game, coordinate, player, param);
    }
}
