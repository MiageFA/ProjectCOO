package controller.display;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.gameService.CreateCityService;

public class CreateCityController implements ActionController {
    private static CreateCityController createCityController = null;

    public static CreateCityController getInstance() {
        if (createCityController == null)
            createCityController = new CreateCityController();
        return createCityController;
    }

    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateCityService createCity = CreateCityService.getInstance();

        return createCity.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        CreateCityService createCity = CreateCityService.getInstance();

        return createCity.execute(game, coordinate, player, param);
    }
}
