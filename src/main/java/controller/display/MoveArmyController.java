package controller.display;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.gameService.MoveArmyService;

public class MoveArmyController implements ActionController {
    private static MoveArmyController moveArmyController = null;

    public static MoveArmyController getInstance() {
        if (moveArmyController == null)
            moveArmyController = new MoveArmyController();
        return moveArmyController;
    }
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        MoveArmyService moveArmy = MoveArmyService.getInstance();

        return moveArmy.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        MoveArmyService moveArmy = MoveArmyService.getInstance();

        return moveArmy.execute(game, coordinate, player, param);
    }

}
