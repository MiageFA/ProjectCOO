package controller.display;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;

public interface ActionController {
    boolean canExecute(Game game, Coordinate coordinate, Player player, Object param);
    boolean execute(Game game, Coordinate coordinate, Player player, Object param);
}
