package controller.display;

import service.gameService.GameService;

public class CommitController {
    private GameService gameService;
    private static CommitController commitController = null;

    public static CommitController getInstance() {
        if (commitController == null)
            commitController = new CommitController();
        return commitController;
    }

    public CommitController()
    {
        gameService = GameService.getInstance();
    }

    public void commit()
    {
        gameService.commit();
    }
}
