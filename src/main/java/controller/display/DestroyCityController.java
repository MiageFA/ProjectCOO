package controller.display;

import bdd.DO.Coordinate;
import bdd.DO.Game;
import bdd.DO.Player;
import service.gameService.DestroyCityService;

public class DestroyCityController implements ActionController {
    private static DestroyCityController destroyCityController = null;

    public static DestroyCityController getInstance() {
        if (destroyCityController == null)
            destroyCityController = new DestroyCityController();
        return destroyCityController;
    }
    public boolean canExecute(Game game, Coordinate coordinate, Player player, Object param) {
        DestroyCityService destroyCity = DestroyCityService.getInstance();

        return destroyCity.canExecute(game, coordinate, player, param);
    }

    public boolean execute(Game game, Coordinate coordinate, Player player, Object param) {
        DestroyCityService destroyCity = DestroyCityService.getInstance();

        return destroyCity.execute(game, coordinate, player, param);
    }
}
