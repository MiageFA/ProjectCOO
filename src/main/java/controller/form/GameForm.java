package controller.form;

public class GameForm {
    private String name;
    private Integer maxNumberPlayers;
    //private State state;

    private Integer initNumberResourcesPerPlayer;
    private Integer numberResourcesPerRound;
    private Integer maxRounds;
    /*number cases between cities at the creation of the game*/
    private Integer initDistanceBetweenCities;
    private Integer roundDurationTime;
    private Integer maxArmiesPerTerritories;
    private Integer widthMap;
    private Integer heightMap;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMaxNumberPlayers() {
		return maxNumberPlayers;
	}
	public void setMaxNumberPlayers(Integer maxNumberPlayers) {
		this.maxNumberPlayers = maxNumberPlayers;
	}
	public Integer getInitNumberResourcesPerPlayer() {
		return initNumberResourcesPerPlayer;
	}
	public void setInitNumberResourcesPerPlayer(Integer initNumberResourcesPerPlayer) {
		this.initNumberResourcesPerPlayer = initNumberResourcesPerPlayer;
	}
	public Integer getNumberResourcesPerRound() {
		return numberResourcesPerRound;
	}
	public void setNumberResourcesPerRound(Integer numberResourcesPerRound) {
		this.numberResourcesPerRound = numberResourcesPerRound;
	}
	public Integer getMaxRounds() {
		return maxRounds;
	}
	public void setMaxRounds(Integer maxRounds) {
		this.maxRounds = maxRounds;
	}
	public Integer getInitDistanceBetweenCities() {
		return initDistanceBetweenCities;
	}
	public void setInitDistanceBetweenCities(Integer initDistanceBetweenCities) {
		this.initDistanceBetweenCities = initDistanceBetweenCities;
	}
	public Integer getRoundDurationTime() {
		return roundDurationTime;
	}
	public void setRoundDurationTime(Integer roundDurationTime) {
		this.roundDurationTime = roundDurationTime;
	}
	public Integer getMaxArmiesPerTerritories() {
		return maxArmiesPerTerritories;
	}
	public void setMaxArmiesPerTerritories(Integer maxArmiesPerTerritories) {
		this.maxArmiesPerTerritories = maxArmiesPerTerritories;
	}
	public Integer getWidthMap() {
		return widthMap;
	}
	public void setWidthMap(Integer widthMap) {
		this.widthMap = widthMap;
	}
	public Integer getHeightMap() {
		return heightMap;
	}
	public void setHeightMap(Integer heightMap) {
		this.heightMap = heightMap;
	}

    

	
}
