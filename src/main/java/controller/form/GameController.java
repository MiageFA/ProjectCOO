package controller.form;

import bdd.DO.Game;
import bdd.DO.Player;
import service.gameService.GameService;
import util.State;

import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class GameController {
	
	private static final GameController INSTANCE=new GameController();
	private GameController(){};
	
	public static GameController getInstance(){
		return INSTANCE;
	}
	
	public void createGame(GameForm gameForm, Player creator){
		Game game=new Game();
		game.setName(gameForm.getName());
		game.setDateCreation(new Date());
		game.setInitDistanceBetweenCities(gameForm.getInitDistanceBetweenCities());
		game.setInitNumberResourcesPerPlayer(gameForm.getInitNumberResourcesPerPlayer());
		game.setMaxNumberPlayers(gameForm.getMaxNumberPlayers());
		game.setNumberResourcesPerRound(gameForm.getNumberResourcesPerRound());
		game.setRoundDurationTime(gameForm.getRoundDurationTime());
		Integer height = gameForm.getHeightMap();
		Integer width = gameForm.getWidthMap();
		game.generateMap(width, height, gameForm.getMaxNumberPlayers());
		
		GameService gameService = new GameService();
		gameService.createGame(game, creator);
		
	}
	
	public List<Game> getGamesWaiting(Player p, State state){
		return GameService.getInstance().getGamesByState(p, state);
	}

	public List<Game> getGamesByCreator(Player player) {
		
		return GameService.getInstance().getGamesByCreator(player);
	}
	
	public List<Game> getGamesToLaunch(Player player){
		return GameService.getInstance().getGamesToLaunch(player);
	}
}
