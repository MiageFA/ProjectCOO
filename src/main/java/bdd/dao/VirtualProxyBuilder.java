package bdd.dao;

import bdd.dao.factory.Factory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class VirtualProxyBuilder<T> implements InvocationHandler {

    private T realObject = null;
    private Factory<T> factory;
    private Class<?> iface;


    public VirtualProxyBuilder(final Class<?> iface, final Factory<T> factory) {
        this.iface = iface;
        this.factory = factory;

    }

    public T getProxy() {
        @SuppressWarnings("unchecked")
        T p = (T) Proxy.newProxyInstance(iface.getClassLoader(), new Class<?>[]{iface}, this);
        return p;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (realObject == null) {
            realObject = (T) factory.create();
        }
        return method.invoke(realObject, args);
    }


}
