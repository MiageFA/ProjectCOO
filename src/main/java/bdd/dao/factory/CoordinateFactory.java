package bdd.dao.factory;

import bdd.DO.Coordinate;
import bdd.dao.mapper.CoordinateMapper;

public class CoordinateFactory extends Factory<Coordinate> {
    private static CoordinateFactory INSTANCE = new CoordinateFactory();

    private CoordinateFactory() {
        this.dataMapper = CoordinateMapper.getInstance();
    }

    public static CoordinateFactory getInstance() {
        return INSTANCE;
    }

}
