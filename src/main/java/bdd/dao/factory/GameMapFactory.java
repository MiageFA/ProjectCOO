package bdd.dao.factory;

import bdd.DO.GameMap;
import bdd.dao.mapper.GameMapMapper;

public class GameMapFactory extends Factory<GameMap> {

    public GameMapFactory(Integer id) {
        this.dataMapper = GameMapMapper.getInstance();
        this.id = id;
    }
}
