package bdd.dao.factory;

import bdd.DO.Player;
import bdd.dao.mapper.list.PlayersMapper;

import java.util.List;

public class PlayersFactory extends Factory<List<Player>> {


    public PlayersFactory(Integer id) {
        this.dataMapper = PlayersMapper.getInstance();
        this.id = id;
    }

}
