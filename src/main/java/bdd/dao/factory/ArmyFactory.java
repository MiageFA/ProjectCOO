package bdd.dao.factory;

import bdd.DO.Army;
import bdd.dao.mapper.ArmyMapper;

public class ArmyFactory extends Factory<Army> {
    private final static ArmyFactory INSTANCE = new ArmyFactory();

    private ArmyFactory() {
        this.dataMapper = ArmyMapper.getInstance();
    }

    public ArmyFactory getInstance() {
        return INSTANCE;
    }


}
