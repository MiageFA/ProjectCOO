package bdd.dao.factory;

import bdd.DO.Territory;
import bdd.dao.mapper.TerritoryMapper;

public class TerritoryFactory extends Factory<Territory> {


    public TerritoryFactory(Integer id) {
        this.dataMapper = TerritoryMapper.getInstance();
        this.id = id;
    }

}
