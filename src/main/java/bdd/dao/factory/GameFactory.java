package bdd.dao.factory;

import bdd.DO.Game;
import bdd.dao.mapper.GameMapper;

public class GameFactory extends Factory<Game> {

    public GameFactory(Integer id) {
        this.dataMapper = GameMapper.getInstance();
        this.id = id;
    }


}
