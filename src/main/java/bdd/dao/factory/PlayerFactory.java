package bdd.dao.factory;

import bdd.DO.Player;
import bdd.dao.mapper.PlayerMapper;

public class PlayerFactory extends Factory<Player> {


    public PlayerFactory(Integer id) {
        this.dataMapper = PlayerMapper.getInstance();
        this.id = id;
    }


}
