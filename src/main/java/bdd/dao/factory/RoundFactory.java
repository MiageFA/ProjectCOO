package bdd.dao.factory;

import bdd.DO.Round;
import bdd.dao.mapper.RoundMapper;

public class RoundFactory extends Factory<Round> {


    private RoundFactory(Integer id) {
        this.dataMapper = RoundMapper.getInstance();
        this.id = id;
    }

}
