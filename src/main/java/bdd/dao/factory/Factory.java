package bdd.dao.factory;

import bdd.dao.mapper.DataMapper;

public abstract class Factory<T> {

	protected DataMapper<T> dataMapper;
	protected Integer id;

	public T create() {
		return dataMapper.findByID(id);
	}

	public DataMapper<T> getDattaMapper() {
		return dataMapper;
	}

	public void setDattaMapper(DataMapper<T> dataMapper) {
		this.dataMapper = dataMapper;
	}

}
