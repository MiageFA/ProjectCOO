package bdd.dao.factory;

import bdd.DO.City;
import bdd.dao.mapper.CityMapper;

public class CityFactory extends Factory<City> {
    private final static CityFactory INSTANCE = new CityFactory();

    private CityFactory() {
        this.dataMapper = CityMapper.getInstance();
    }

    public static CityFactory getInstance() {
        return INSTANCE;
    }
}
