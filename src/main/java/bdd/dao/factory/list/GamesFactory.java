package bdd.dao.factory.list;

import bdd.DO.Game;
import bdd.dao.factory.Factory;
import bdd.dao.mapper.list.GamesMapper;

import java.util.List;

public class GamesFactory extends Factory<List<Game>> {


    public GamesFactory(Integer id) {
        this.dataMapper = GamesMapper.getInstance();
        this.id = id;
    }

}
