package bdd.dao.mapper.list;

import bdd.DBConnection;
import bdd.DO.Player;
import bdd.dao.mapper.DataMapper;
import bdd.dao.mapper.PlayerMapper;

import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlayersMapper extends DataMapper<List<Player>> {
	private static PlayersMapper INSTANCE = new PlayersMapper();

	private PlayersMapper() {
	}

	public static PlayersMapper getInstance() {
		return INSTANCE;
	}

	@Override
	public List<Player> getEmptyObject() {
		List<Player> list = new ArrayList<Player>();
		return list;
	}

	@Override
	public List<Player> complete(List<Player> object) {
		PlayerMapper playerMapper = PlayerMapper.getInstance();
		for (Player p : object) {
			playerMapper.complete(p);
		}
		return object;
	}

	@Override
	public List<Player> findByID(final Integer idGame) {
		if (session.containsKey(idGame) && session.get(idGame).get()!=null) {
			return session.get(idGame).get();
		}
		final StringBuilder req = new StringBuilder("select * from player_in_game pg")
				.append(" left join player p on pg.id_player=p.id").append(" where id_game=?");
		final PlayerMapper playerMapper = PlayerMapper.getInstance();
		final List<Player> players = new ArrayList<Player>();
		try {
			connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req.toString());
			ps.setInt(1, idGame);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Player player = playerMapper.getEmptyObject();
				player.setId(rs.getInt("p.id"));
				for (Map.Entry<String, String> att : playerMapper.getAttributs().entrySet()) {

					Method mget = playerMapper.getMaClass().getMethod("get" + att.getKey());
					Class<?> attrributClass = mget.getReturnType();
					Method mset = playerMapper.getMaClass().getMethod("set" + att.getKey(), attrributClass);

					setData(player, rs, attrributClass.getName(), att.getValue(), mset);

				}
				playerMapper.complete(player);
				players.add(player);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}

	@Override
	public void nextUpdate(List<Player> o) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextInsert(List<Player> o) {
		// TODO Auto-generated method stub

	}

}
