package bdd.dao.mapper.list;

import bdd.DBConnection;
import bdd.DO.Game;
import bdd.dao.mapper.DataMapper;
import bdd.dao.mapper.GameMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GamesMapper extends DataMapper<List<Game>> {

    private static GamesMapper INSTANCE = new GamesMapper();

    private GamesMapper() {
    }

    public static GamesMapper getInstance() {
        return INSTANCE;
    }
    
	@Override
    public List<Game> findByID(final Integer idPlayer) {
        if (session.containsKey(idPlayer) && session.get(idPlayer).get()!=null) {
            return session.get(idPlayer).get();
        }
        final StringBuilder req = new StringBuilder("select * from player_in_game pg")
                .append(" where id_player=?");
        final GameMapper gameMapper = GameMapper.getInstance();
        final List<Game> games = new ArrayList<Game>();
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req.toString());
            ps.setInt(1, idPlayer);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Game game = gameMapper.findByID(rs.getInt("id_game"));
                games.add(game);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return games;
    }

    @Override
    public List<Game> getEmptyObject() {

        return null;
    }

    @Override
    public List<Game> complete(List<Game> object) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void nextUpdate(List<Game> o) {
        // TODO Auto-generated method stub

    }

    @Override
    public void nextInsert(List<Game> o) {
        // TODO Auto-generated method stub

    }

}
