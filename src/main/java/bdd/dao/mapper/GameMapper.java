package bdd.dao.mapper;

import bdd.DBConnection;
import bdd.DO.Game;
import bdd.DO.GameMap;
import bdd.DO.Player;
import bdd.dao.VirtualProxyBuilder;
import bdd.dao.factory.GameMapFactory;
import bdd.dao.factory.PlayerFactory;
import bdd.dao.factory.PlayersFactory;
import bdd.dao.mapper.list.PlayersMapper;
import bdd.dao.mapper.util.StateUtil;
import util.State;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class GameMapper extends DataMapper<Game> {

    private static GameMapper INSTANCE = new GameMapper();
    private final Logger LOGGER = Logger.getGlobal();

    private GameMapper() {
        this.table = "game";
        this.maClass = Game.class;
        this.attributs = new HashMap<String, String>();
        this.attributs.put("Name", "name");
        this.attributs.put("DateCreation", "date_creation");
        this.attributs.put("DateBegin", "date_begin");
        this.attributs.put("DateEnd", "date_end");
        this.attributs.put("MaxNumberPlayers", "max_number_players");
        this.attributs.put("InitNumberResourcesPerPlayer", "init_number_resources_per_player");
        this.attributs.put("NumberResourcesPerRound", "number_resources_per_round");
        this.attributs.put("InitDistanceBetweenCities", "init_distance_between_cities");
        this.attributs.put("RoundDurationTime", "round_duration_time");
        this.attributs.put("IdWinner", "player_winner");

    }

    public static GameMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public Game getEmptyObject() {
        Game game = new Game();
        return game;
    }

    @Override
    public Game complete(Game object) {
        object.setPlayers(
                (List<Player>) new VirtualProxyBuilder<List<Player>>(List.class, new PlayersFactory(object.getId()))
                        .getProxy());
        final Integer idMap = getIdMapByGame(object.getId());
        if (idMap != null && idMap != 0) {
            object.setMap(GameMapMapper.getInstance().findByID(idMap));
            object.setResources(getResources(object.getId()));
            object.setState(StateUtil.getState(getIdState(object.getId())));
            object.setCurrentRound(RoundMapper.getInstance().getCurruntRound(object));
            object.setCreator(getCreator(object));
        }
        return object;
    }

    public Player getCreator(Game game) {
        String req = "select player_creator from " + this.table + " where id=?";
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, game.getId());

            LOGGER.info(ps.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return PlayerMapper.getInstance().findByID(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * returns the state id of game
     *
     * @param idGame
     * @return
     */
    private Integer getIdState(Integer idGame) {
        String req = "select id_state from " + this.table + " where id=?";
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idGame);

            LOGGER.info(ps.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("id_state");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * get id map by game
     *
     * @param idGame
     * @return
     */
    private Integer getIdMapByGame(Integer idGame) {
        try {

            final String req = "select id_map from " + this.table + " where id=?";
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idGame);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int id = rs.getInt("id_map");
            return id;
        } catch (Exception e) {
            System.out.println("GameMaper : getIdMapByGame");
            e.printStackTrace();
        }
        return null;
    }

    private Map<Player, Integer> getResources(Integer idGame) {
        Map<Player, Integer> resources = new HashMap<Player, Integer>();
        try {
            final String req = "select id_player,resources from player_in_game where id_game=?";
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idGame);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer idPlayer = rs.getInt("id_player");
                Player player = new PlayerFactory(idPlayer).create();
                Integer res = rs.getInt("resources");
                resources.put(player, res);
            }

        } catch (Exception e) {
            System.out.println("GameMapper: getResources");
            e.printStackTrace();
        }
        return resources;
    }

    @Override
    public void nextUpdate(Game o) {
        List<Player> playersInDB = PlayersMapper.getInstance().findByID(o.getId());
        for (Player player : o.getPlayers()) {
            if (!playersInDB.contains(player)) {
                addPlayerInGame(player.getId(), o.getId(), o.getResources().get(player));
            }
            updateResources(player.getId(), o.getId(), o.getResources().get(player));
        }
        for (Player player : playersInDB) {
            if (!o.getPlayers().contains(player)) {
                deletePlayerInGame(player.getId(), o.getId());
            }
        }
        try{
        	String req = "update "+this.table+ " set id_state=? where id = ?";
        	Connection connection= DBConnection.getConnection();
        	PreparedStatement ps = connection.prepareStatement(req);
        	ps.setInt(1, StateUtil.getStateId(o.getState()));
        	ps.setInt(2,o.getId());
        	LOGGER.info(ps.toString());
        	ps.executeUpdate();
        }
        catch(SQLException e){
        	e.printStackTrace();
        }
    }

    /**
     * tells us if player is in game
     *
     * @param idPlayer
     * @param idGame
     * @return
     */
    public Boolean playerInGame(Integer idPlayer, Integer idGame) {
        boolean bool = false;
        final String req = "select * from player_in_game where id_player=? and id_game=?";
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idPlayer);
            ps.setInt(2, idGame);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                bool = true;
            } else {
                bool = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bool;
    }

    /**
     * adds the player on the game, player participate for the game
     *
     * @param idPlayer
     * @param idGame
     * @param resource
     */
    public void addPlayerInGame(final Integer idPlayer, final Integer idGame, final Integer resource) {
        final String req = "insert into player_in_game(id_player,id_game,resources) values (?,?,?)";
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idPlayer);
            ps.setInt(2, idGame);

            ps.setInt(3, resource);
            LOGGER.info(ps.toString());
            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deletePlayerInGame(Integer idPlayer, Integer idGame) {
        final String req = "delete from player_in_game where id_player=? and id_game=?";
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idPlayer);
            ps.setInt(2, idGame);
            LOGGER.info(ps.toString());
            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * updates resources for the player given and on the game given
     *
     * @param idPlayer  : id player
     * @param idGame    : id game
     * @param resources
     */
    public void updateResources(final Integer idPlayer, final Integer idGame, final Integer resources) {
        final String req = "update player_in_game set resources=? where id_player=? and id_game=?";
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, resources);
            ps.setInt(2, idPlayer);
            ps.setInt(3, idGame);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void nextInsert(Game o) {
        try {

            GameMap map = o.getMap();
            if (map != null) {
                Integer idMap = GameMapMapper.getInstance().insert(map);
                map.setId(idMap);
                String req = "update " + this.table + " set id_map=? , id_state=?,player_creator=? where id=?";
                connection = DBConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(req);
                ps.setInt(1, idMap);
                ps.setInt(2, StateUtil.getStateId(o.getState()));
                ps.setInt(3, o.getCreator().getId());
                ps.setInt(4, o.getId());
                LOGGER.info(ps.toString());
                ps.executeUpdate();
                connection.close();
            }

        } catch (Exception e) {
            System.out.println("GameMapper: nextInsert");
            e.printStackTrace();
        }
    }

    /**
     * return games by state
     *
     * @param p     : current player
     * @param state : state to find
     * @return game list
     */
    public List<Game> getGamesByState(Player p, State state) {
        List<Game> games = new ArrayList<Game>();
        String req = "select id from " + this.table
                + " where id_state=?";
        if (state == State.IN_PROGRESS)
            req += " and id in (select id_game from player_in_game where id_player=?)";
        if (state == State.WAITING)
            req += " and id not in (select id_game from player_in_game where id_player=? )";
        Connection connection = DBConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,  StateUtil.getStateId(state));
            ps.setInt(2,p.getId());
            LOGGER.info(ps.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                games.add(this.findByID(rs.getInt("id")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return games;
    }

    
    public List<Game> getGamesByCreator(Player player){
    	List<Game> games = new ArrayList<>();
    	String req = "select id from "+this.table+" where player_creator=?";
    	try{
    		Connection connection = DBConnection.getConnection();
    		PreparedStatement ps = connection.prepareStatement(req);
    		ps.setInt(1, player.getId());
    		ResultSet rs = ps.executeQuery();
    		while(rs.next()){
    			Integer idGame =rs.getInt("id");
    			games.add(this.findByID(idGame));
    		}
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
    	return games;
    
    }
}
