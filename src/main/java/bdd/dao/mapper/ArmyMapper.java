package bdd.dao.mapper;

import bdd.DBConnection;
import bdd.DO.Army;
import bdd.DO.Territory;
import bdd.dao.factory.PlayerFactory;
import bdd.dao.factory.TerritoryFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class ArmyMapper extends DataMapper<Army> {
	private Logger LOGGER = Logger.getLogger(this.getClass().getName());
    private static ArmyMapper INSTANCE = new ArmyMapper();

    private ArmyMapper() {
        this.table = "army";
        this.maClass = Army.class;
        this.attributs = new HashMap<String, String>();
    }

    public static ArmyMapper getInstance() {
        return INSTANCE;
    }
    
    @Override
    public Integer insert(Army army) {
        Integer id = 0;
        try {
            String req = "insert into army(id_player,id_territory) values(?,?)";
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, army.getPlayer().getId());
            ps.setInt(2, army.getTerritory().getId());
            LOGGER.info(ps.toString());
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;

    }

    @Override
    public Army getEmptyObject() {
        return new Army();
    }
    
    @Override
    public Army findByID(Integer id) {
        if (session.containsKey(id)) {
            return session.get(id).get();
        }
        try {
            String req = "select id_player,id_territory from " + this.table + " where id=?";
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Army army = new Army();
                army.setId(id);
                army.setPlayer(new PlayerFactory(rs.getInt("id_player")).create());
                army.setTerritory(new TerritoryFactory(rs.getInt("id_territory")).create());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public void update(Army army) {
        try {

            String req = "update army set id_territory=? and id_player=? where id=?";
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, army.getPlayer().getId());
            ps.setInt(1, army.getTerritory().getId());
            ps.setInt(3, army.getId());
            ps.execute();
        } catch (Exception e) {
            System.out.println("ArmyMapper : update");
            e.printStackTrace();
        }

    }

    @Override
    public Army complete(Army object) {
        return null;
    }

    @Override
    public void nextUpdate(Army o) {
        return;

    }

    @Override
    public void nextInsert(Army o) {
        // TODO Auto-generated method stub

    }

	public List<Army> findByTeritory(Territory territory) {
		List<Army> armies = new ArrayList<>();
		String req = "select id,id_player from army where id_territory=?";
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req);
			ps.setInt(1, territory.getId());
			LOGGER.info(ps.toString());
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Army army = new Army();
				army.setId(rs.getInt("id"));
				army.setTerritory(territory);
				army.setPlayer(PlayerMapper.getInstance().findByID(rs.getInt("id_player")));
				armies.add(army);
			}
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return armies;
	}

}
