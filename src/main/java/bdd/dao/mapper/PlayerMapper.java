package bdd.dao.mapper;

import bdd.DBConnection;
import bdd.DO.Game;
import bdd.DO.Player;
import bdd.dao.VirtualProxyBuilder;
import bdd.dao.factory.list.GamesFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class PlayerMapper extends DataMapper<Player> {
	private final Logger LOGGER =Logger.getGlobal();
    private static PlayerMapper INSTANCE = new PlayerMapper();

    public PlayerMapper() {
        this.table = "player";
        this.maClass = Player.class;
        this.attributs = new HashMap<String, String>();
        this.attributs.put("FirstName", "first_name");
        this.attributs.put("LastName", "last_name");
        this.attributs.put("Pseudo", "pseudo");
        this.attributs.put("Password", "password");
    }

    public static PlayerMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public Player getEmptyObject() {
        Player player = new Player();
        return player;
    }

    @Override
    public Player complete(Player object) {
        object.setGames((List<Game>) new VirtualProxyBuilder<List<Game>>(List.class, new GamesFactory(object.getId())).getProxy());
        return object;
    }

    @Override
    public void nextUpdate(Player o) {
        return;

    }

    @Override
    public void nextInsert(Player o) {
        // TODO Auto-generated method stub

    }
    
    public Player findByLogin(final String login){
    	String req = "SELECT id FROM "+this.table+" WHERE pseudo= ?";
    	Connection connection=DBConnection.getConnection();
    	try{
        	PreparedStatement ps=connection.prepareStatement(req);
        	ps.setString(1,login);
        	LOGGER.info(ps.toString());
        	ResultSet rs =ps.executeQuery();
        	if(rs.next()){
        		Integer id=rs.getInt("id");
        		return this.findByID(id);
        	}
        	
    	}
    	catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return null;
    }

}
