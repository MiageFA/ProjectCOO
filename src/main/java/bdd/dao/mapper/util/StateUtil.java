package bdd.dao.mapper.util;

import util.State;

public class StateUtil {
	
	public static Integer getStateId(State state){
		if(State.WAITING.equals(state)){
			return 1;
		}
		else if(State.IN_PROGRESS.equals(state)){
			return 2;
		}
		else if(State.FINISHED.equals(state)){
			return 3;
		}
		return 0;
	}
	
	public static State getState(Integer id){
		if(id==1){
			return State.WAITING;
		}
		else if(id==2){
			return State.IN_PROGRESS;
		}
		else if(id==3){
			return State.FINISHED;
		}
		return null;
	}
}
