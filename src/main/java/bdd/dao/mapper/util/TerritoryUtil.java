package bdd.dao.mapper.util;

import bdd.DO.TerritoryTypeEnum;

public class TerritoryUtil {
	
	public static Integer  getTerritoryTypeId(TerritoryTypeEnum type){
		if(type.equals(TerritoryTypeEnum.MOUNTAIN)){
			return 1;
		}
		else if(type.equals(TerritoryTypeEnum.FIELD)){
			return 2;
		}
		else if(type.equals(TerritoryTypeEnum.PLAIN)){
			return 3;
		}
		return 0;
	}
	
	public static TerritoryTypeEnum getTerritoryType(Integer idType){
		switch (idType){
			case 1:
				return TerritoryTypeEnum.MOUNTAIN;
			case 2:
				return TerritoryTypeEnum.FIELD;
			case 3:
				return TerritoryTypeEnum.PLAIN;
		}
		return null;
	}
	
}
