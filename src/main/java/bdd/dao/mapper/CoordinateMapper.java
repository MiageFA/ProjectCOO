package bdd.dao.mapper;

import bdd.DO.Coordinate;

import java.util.HashMap;

public class CoordinateMapper extends DataMapper<Coordinate> {

    private final static CoordinateMapper INSTANCE = new CoordinateMapper();

    private CoordinateMapper() {
        this.table = "coordinate";
        this.maClass = Coordinate.class;
        this.attributs = new HashMap<String, String>();
        this.attributs.put("X", "x");
        this.attributs.put("Y", "y");
    }

    public static CoordinateMapper getInstance() {
        return INSTANCE;
    }


    @Override
    public Coordinate getEmptyObject() {
        
        return new Coordinate();
    }

    @Override
    public Coordinate complete(Coordinate object) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void nextUpdate(Coordinate o) {
        return;

    }

    @Override
    public void nextInsert(Coordinate o) {
        // TODO Auto-generated method stub

    }


}
