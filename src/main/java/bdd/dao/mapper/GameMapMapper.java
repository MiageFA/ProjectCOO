package bdd.dao.mapper;

import bdd.DO.Coordinate;
import bdd.DO.GameMap;
import bdd.DO.Territory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameMapMapper extends DataMapper<GameMap> {

    private static GameMapMapper INSTANCE = new GameMapMapper();

    private GameMapMapper() {
        this.table = "map";
        this.maClass = GameMap.class;
        this.attributs = new HashMap<String, String>();
        attributs.put("Height", "height");
        attributs.put("Width", "width");
    }

    public static GameMapMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public GameMap getEmptyObject() {
        return new GameMap();
    }

    @Override
    public GameMap complete(GameMap object) {
        final Map<Coordinate, Territory> map = new HashMap<Coordinate, Territory>();
        final List<Territory> territories = TerritoryMapper.getInstance().getTerritoriesByMap(object.getId());
        for (Territory t : territories) {
            map.put(t.getCoordinate(), t);
        }
        object.setMap(map);
        return object;

    }


    @Override
    public void nextUpdate(GameMap o) {
        return;

    }


    @Override
    public void nextInsert(GameMap o) {
        try {
            CoordinateMapper coordinateMapper = CoordinateMapper.getInstance();
            TerritoryMapper territoryMapper = TerritoryMapper.getInstance();
            for (Map.Entry<Coordinate, Territory> entry : o.getMap().entrySet())
            {
                Coordinate coordinate = entry.getKey();
                Territory territory = entry.getValue();
                if (coordinate != null && territory != null)
                {
                    Integer id = coordinateMapper.insert(coordinate);
                    coordinate.setId(id);
                    id = territoryMapper.insert(territory);
                    territory.setId(id);
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
