package bdd.dao.mapper;

import bdd.DBConnection;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public abstract class DataMapper<T> {
	protected Map<Integer, WeakReference<T>> session = new HashMap<Integer, WeakReference<T>>();

	protected String table;
	protected Map<String, String> attributs;
	protected Class<T> maClass;
	protected Connection connection;
	private Logger LOGGER = Logger.getGlobal();

	/**
	 * find the object having the id given on parameter returns null if No
	 * object Found
	 *
	 * @param id:
	 *            the if the instance
	 * @return
	 */
	public T findByID(Integer id) {
		if (session.containsKey(id) && session.get(id).get()!=null) {
			return session.get(id).get();
		}
		try {

			final String req = "select * from " + this.table + " where id=?";
			final T o = this.getEmptyObject();
			Method setId = maClass.getMethod("setId", Integer.class);
			
			connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req);
			ps.setInt(1, id);
			LOGGER.info(ps.toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				for (Map.Entry<String, String> att : attributs.entrySet()) {

					Method mget = maClass.getMethod("get" + att.getKey());
					Class<?> attrributClass = mget.getReturnType();
					Method mset = maClass.getMethod("set" + att.getKey(), attrributClass);
					setData(o, rs, attrributClass.getName(), att.getValue(), mset);

				}
				setId.invoke(o, id);
				session.put(id, new WeakReference<T>(o));
				this.complete(o);
				return o;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * set the data getting on select request to the object
	 * 
	 * @param o
	 *            : the object of domain
	 * @param rs
	 *            : the result set getting on the select request
	 * @param className
	 *            : the class name of the attribute
	 * @param att
	 *            : the value of the attribute
	 * @param setter
	 *            : the setter the attribute
	 */
	protected void setData(Object o, ResultSet rs, String className, String att, Method setter) {
		try {
			if ("java.lang.String".equals(className)) {
				setter.invoke(o, rs.getString(att));
			} else if ("java.lang.Integer".equals(className)) {
				setter.invoke(o, rs.getInt(att));
			} else if ("java.lang.Double".equals(className)) {
				setter.invoke(o, rs.getDouble(att));
			} else if ("java.lang.Long".equals(className)) {
				setter.invoke(o, rs.getLong(att));
			} else if ("java.util.Date".equals(className)) {
				setter.invoke(o, rs.getDate(att));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * insert the object given on data base
	 * 
	 * @param obj
	 * @return
	 */
	public Integer insert(T obj) {
		int id = 0;
		try {

			connection = DBConnection.getConnection();
			String req = "Insert INTO " + table + " (";
			List<String> keys = new ArrayList<String>(attributs.keySet());
			if (!keys.isEmpty()) {
				req += attributs.get(keys.get(0));
				int i = 1;
				while (i < keys.size()) {
					req += " , " + attributs.get(keys.get(i));
					i++;
				}

				req += " ) values (?";
				i = 1;
				while (i < keys.size()) {
					req += ",?";
					i++;
				}
				req += ");";
				PreparedStatement ps = connection.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
				i = 1;
				setAttributes(i, ps, obj, keys);
				LOGGER.info(ps.toString());
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					id = rs.getInt(1);
					Method setId = maClass.getMethod("setId", Integer.class);
					setId.invoke(obj, id);
				}
			}

			connection.close();
			nextInsert(obj);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * sets the object to the prepared statement the value of the parameter on
	 * the position i
	 * 
	 * @param i
	 *            : the position where the value(object) will be inserted
	 * @param ps
	 *            : the prepared statement
	 * @param o
	 *            (String, Integer, Date ...)
	 */
	protected void setParamToPS(int i, PreparedStatement ps, Object o) {
		try {

			if (o == null) {
				ps.setString(i, null);
			}
			if (o instanceof String) {
				ps.setString(i, (String) o);
			} else if (o instanceof Integer) {
				ps.setInt(i, (Integer) o);
			} else if (o instanceof Double) {
				ps.setDouble(i, (Double) o);
			} else if (o instanceof Float) {
				ps.setFloat(i, (Float) o);
			} else if (o instanceof Long) {
				ps.setLong(i, (Long) o);
			} else if (o instanceof Date) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String strDate = formatter.format((Date)o);
				ps.setString(i, strDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * updates the object (of domain) given on data base
	 * 
	 * @param object
	 */
	public void update(final T object) {
		try {
			connection = DBConnection.getConnection();
			String req = "UPDATE " + this.table + " set ";
			List<String> attrs = new ArrayList<String>(attributs.keySet());
			// rajouter un if pour get 0 s'il existe
			req += " " + this.attributs.get(attrs.get(0)) + "=? ";
			int i = 1;
			while (i < attrs.size()) {
				req += ", " + this.attributs.get(attrs.get(i)) + "=? ";
				i++;
			}
			req += " where id=?";
			PreparedStatement ps = connection.prepareStatement(req);
			i = 1;
			setAttributes(i, ps, object, attrs);
			Method m = maClass.getMethod("getId");
			Object id = m.invoke(object);
			ps.setInt(attrs.size() + 1, (Integer) id);
			LOGGER.info(ps.toString());
			ps.execute();
			this.nextUpdate(object);
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * sets the attributes to the prepared statement
	 * 
	 * @param i
	 * @param ps
	 * @param object
	 * @param attrs
	 */
	private void setAttributes(int i, PreparedStatement ps, Object object, List<String> attrs) {
		try {
			while (i <= attrs.size()) {
				Method m = maClass.getMethod("get" + attrs.get(i - 1));
				Object o = m.invoke(object);
				setParamToPS(i, ps, o);
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * deletes the object (of domain) given from the data base
	 * 
	 * @param object
	 */
	public void delete(T object) {
		try {
			String req = "DELETE FROM " + this.table + " where id =?";
			Connection connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req);
			Method m = maClass.getMethod("getId");
			Object id = m.invoke(object);
			ps.setInt(1, (Integer) id);
			ps.execute();
		} catch (Exception e) {
			System.out.println("Datamapper: delete");
			e.printStackTrace();
		}
	}

	protected abstract T getEmptyObject();
	
	/**
	 * complete the object, this method is called at the the of findById 
	 * loads and sets  the specific attributes of the object  
	 * @param object
	 * @return
	 */
	protected abstract T complete(T object);
	
	/**completes the specific update after update
	 * it's specific for each object
	 * @param o
	 */
	protected abstract void nextUpdate(T o);
	
	/**
	 * completes the specific insert after insert
	 * it's specific for each object
	 * @param o
	 */
	protected abstract void nextInsert(T o);

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public Map<String, String> getAttributs() {
		return attributs;
	}

	public void setAttributs(Map<String, String> attributs) {
		this.attributs = attributs;
	}

	public Class<T> getMaClass() {
		return maClass;
	}

	public void setMaClass(Class<T> maClass) {
		this.maClass = maClass;
	}

}
