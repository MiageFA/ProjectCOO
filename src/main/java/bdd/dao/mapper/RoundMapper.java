package bdd.dao.mapper;

import bdd.DBConnection;
import bdd.DO.Game;
import bdd.DO.Player;
import bdd.DO.Round;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class RoundMapper extends DataMapper<Round> {
	private static RoundMapper INSTANCE = new RoundMapper();
	private Logger LOGGER = Logger.getLogger(this.getClass().getName());

	private RoundMapper() {
		this.table = "round";
		this.maClass = Round.class;
		this.attributs = new HashMap<String, String>();
		this.attributs.put("RoundNumber", "round_number");
		this.attributs.put("DateBegin", "date_begin");
		this.attributs.put("DateEnd", "date_end");

	}

	public static RoundMapper getInstance() {
		return INSTANCE;
	}

	@Override
	public Round getEmptyObject() {
		return new Round();
	}

	@Override
	public Round complete(Round object) {
		object.setPlayers(getPlayersByRound(object));
		object.setGame(getGameByRound(object));
		return object;
	}

	private Game getGameByRound(Round round) {
		try {
			final String req = "select id_game from " + this.table + " where id=?";
			Connection connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req);
			ps.setInt(1, round.getId());

			LOGGER.info(ps.toString());

			ResultSet rs = ps.executeQuery();
			rs.next();
			Integer idGame = rs.getInt("id_game");
			connection.close();
			return GameMapper.getInstance().findByID(idGame);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<Player> getPlayersByRound(Round round) {
		final List<Player> players = new ArrayList<Player>();
		try {
			final StringBuilder req = new StringBuilder("select * from player_in_round pr")
					.append(" left join player p on pr.id_player=p.id").append(" where id_round=?");
			final PlayerMapper playerMapper = PlayerMapper.getInstance();
			connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req.toString());
			ps.setInt(1, round.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Player player = playerMapper.findByID(rs.getInt("p.id"));
				players.add(player);
			}
			connection.close();
		} catch (Exception e) {
			System.out.println("RoundMapper : getPlayersByRound");
			e.printStackTrace();
		}
		return players;
	}

	@Override
	public void nextUpdate(Round o) {
		return;

	}

	@Override
	public void nextInsert(Round o) {
		String req = "UPDATE " + this.table + " set id_game=? where id = ?";
		connection = DBConnection.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement(req);
			ps.setInt(1, o.getGame().getId());
			ps.setInt(2, o.getId());
			LOGGER.info(ps.toString());
			ps.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}

	public Round getCurruntRound(Game game) {
		String req = "select id from " + this.table + " where id_game = ? order by date_begin limit 1";
		try {
			connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(req);
			ps.setInt(1, game.getId());
			LOGGER.info(ps.toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return this.findByID(rs.getInt("id"));
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
