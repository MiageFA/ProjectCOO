package bdd.dao.mapper;

import bdd.DBConnection;
import bdd.DO.City;
import bdd.dao.factory.PlayerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

public class CityMapper extends DataMapper<City> {
    private static CityMapper INSTANCE = new CityMapper();

    private CityMapper() {
        this.table = "city";
        this.maClass = City.class;
        this.attributs = new HashMap<String, String>();
        this.attributs.put("Name", "name");
    }

    public static CityMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public City getEmptyObject() {
        return new City();
    }

    @Override
    public City complete(City city) {
        try {
            String req = "select id_player from " + this.table + " where id=?";
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, city.getId());
            ResultSet rs = ps.executeQuery();
            rs.next();
            Integer idPlayer = rs.getInt("id_player");
            city.setPlayer(PlayerMapper.getInstance().findByID(idPlayer));
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return city;

    }
    
    @Override
    public Integer insert(City city) {
        Integer id = 0;
        try {
            String req = "insert into " + this.table + "(name,id_player ) values (?,?)";
            Connection connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, city.getName());
            ps.setInt(2, city.getPlayer().getId());
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("CityMapper : insert");
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public void nextUpdate(City o) {
        return;

    }

    @Override
    public void nextInsert(City o) {
        // TODO Auto-generated method stub

    }


}
