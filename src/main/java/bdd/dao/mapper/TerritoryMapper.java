package bdd.dao.mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import bdd.DBConnection;
import bdd.DO.Army;
import bdd.DO.Coordinate;
import bdd.DO.Territory;
import bdd.dao.mapper.util.TerritoryUtil;

public class TerritoryMapper extends DataMapper<Territory> {
	private Logger LOGGER = Logger.getLogger(this.getClass().getName());
    private static final TerritoryMapper INSTANCE = new TerritoryMapper();

    private TerritoryMapper() {
        this.table = "territory";
        this.maClass = Territory.class;
        this.attributs = new HashMap<String, String>();
        this.attributs.put("Height", "height");
        this.attributs.put("Width", "width");
        this.attributs.put("MaxNumberArmies", "max_number_armies");
    }

    public static TerritoryMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public Territory getEmptyObject() {
        return new Territory();
    }

    @Override
    public Territory complete(Territory object) {
        String req = "SELECT id_coordinate,id_city,id_territory_type FROM "+this.table+" where id =?";
        try{
            Connection connection =DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,object.getId());
            ResultSet rs = ps.executeQuery();
            rs.next();
            Integer idTerritoryType = rs.getInt("id_territory_type");
            object.setType(TerritoryUtil.getTerritoryType(idTerritoryType));
            object.setCity(CityMapper.getInstance().findByID(rs.getInt("id_city")));
            object.setArmies(ArmyMapper.getInstance().findByTeritory(object));
            object.setCoordinate(CoordinateMapper.getInstance().findByID(rs.getInt("id_coordinate")));
            connection.close();
        }
        catch (SQLException e) {
			System.out.println("---TerritoryMapper: complete---");
			e.printStackTrace();
		}

        return object;
    }

    public List<Territory> getTerritoriesByMap(final Integer id_map) {
        final StringBuilder req = new StringBuilder("select * from ").append(this.table + " t").
                append(" left join map on t.id_map=map.id")
                .append(" where id_map=?");
        List<Territory> territories = new ArrayList<Territory>();
        try {
            connection = DBConnection.getConnection();
            PreparedStatement ps = connection.prepareStatement(req.toString());
            ps.setInt(1, id_map);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
            	Territory t=this.findByID(rs.getInt("t.id"));
                territories.add(t);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return territories;
    }

    @Override
    public void nextUpdate(Territory o) {
    	
    	// updates city 
    	int idCity=0;
        if(o.getCity()!=null &&(o.getCity().getId()==null || CityMapper.getInstance().findByID(o.getCity().getId())==null)){
        	idCity=CityMapper.getInstance().insert(o.getCity());
        	o.getCity().setId(idCity);
        }else if(o.getCity().getId()!=null){
        	idCity=o.getCity().getId();
        }
    	try{
    		final String req = "update "+this.table+" set id_city=? where id=?";
    		Connection conn = DBConnection.getConnection();
    		PreparedStatement ps = conn.prepareStatement(req);
    		ps.setInt(1, idCity);
    		ps.setInt(2, o.getId());
    		LOGGER.info(ps.toString());
    		ps.executeUpdate();
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
    }

    @Override
    public void nextInsert(Territory o) {
        Coordinate c = o.getCoordinate();
        c.setId(CoordinateMapper.getInstance().insert(c));
        Integer idTerritoryType = TerritoryUtil.getTerritoryTypeId(o.getTerritoryType());
        Integer idMap = 0;
        Integer idCity=0;
        if(o.getCity()!=null){
        	idCity=o.getCity().getId();
        }
        if(o.getMap()!=null){
        	idMap=o.getMap().getId();
        }
        String req = "UPDATE Territory set id_territory_type=? , id_city=?, id_coordinate=? , id_map=? where id=?";
        
        
        try{
        	Connection connection =DBConnection.getConnection();
        	PreparedStatement ps = connection.prepareStatement(req);
        	ps.setInt(1,idTerritoryType);
        	ps.setInt(2, idCity);
        	ps.setInt(3, c.getId());
        	ps.setInt(4, idMap);
        	ps.setInt(5, o.getId());
        	ps.executeUpdate();
        	connection.close();
        }
        catch (SQLException e) {
			e.printStackTrace();
		}
        
        

    }

}
