package bdd;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
    protected static String url = "jdbc:mysql://localhost:3306/project_coo?serverTimezone=UTC&useSSL=false";
    protected static String user = "root";
    protected static String password = "";

//  protected static String url = "jdbc:mysql://webtp.fil.univ-lille1.fr/phpmyadmin/project_coo?serverTimezone=UTC&useSSL=false";
//  protected static String user = "root";
//  protected static String password = "";

    static public Connection getConnection() {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
