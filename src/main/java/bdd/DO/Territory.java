package bdd.DO;

import bdd.dao.mapper.TerritoryMapper;
import model.ADomainObject;
import util.Visitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Territory extends ADomainObject {
    private Integer id;
    private Integer maxNumberArmies;
    private Double height;
    private Double width;
    private Coordinate coordinate;
    private Player controller;
    private List<Army> armies;
    private City city;
    private TerritoryTypeEnum type;
    private GameMap map;
    private TerritoryMapper territoryMapper;
    
    public Territory() {
        territoryMapper = TerritoryMapper.getInstance();
        armies = new ArrayList<>();
    }

    public Territory(City city, List<Army> armies, int maxNumberArmies, Player controller, TerritoryTypeEnum type, Coordinate coordinate) {
        super();
        territoryMapper = TerritoryMapper.getInstance();
        this.city = city;
        this.armies = armies;
        this.maxNumberArmies = maxNumberArmies;
        this.controller = controller;
        this.type = type;
        this.coordinate = coordinate;
    }

    public Territory(int maxNumberArmies, TerritoryTypeEnum type, Coordinate coordinate) {
        super();
        territoryMapper = TerritoryMapper.getInstance();
        this.armies = new ArrayList<>();
        this.maxNumberArmies = maxNumberArmies;
        this.controller = null;
        this.city = null;
        this.controller = null;
        this.type = type;
        this.coordinate = coordinate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Territory other = (Territory) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


    public boolean addArmy(Army army) {
        boolean res = this.armies.add(army);
        notifyAllObservers();
        return res;
    }

    public boolean removeArmy(Army army)
    {
        boolean res = this.armies.remove(army);
        notifyAllObservers();
        return res;
    }

    public Map<Player, Long> getNbArmiesByPlayer() {
        Map<Player, Long> nbArmiesByPlayer = new HashMap<>();
        nbArmiesByPlayer = this.armies.stream().collect(Collectors.groupingBy(a -> a.getPlayer(), Collectors.counting()));
        return nbArmiesByPlayer;
    }

    /**
     * Each time territory is modified, compute if controller has changed
     * @return
     */
    public Player reevaluateController() {
        Player newController = this.controller;
        Map<Player, Long> nbArmiesByPlayer = new HashMap<>();
        if (this.city != null)
            return this.city.getPlayer();
        //on compte le nombre d'armée par players
        nbArmiesByPlayer = this.armies.stream().collect(Collectors.groupingBy(a -> a.getPlayer(), Collectors.counting()));
        Long most = new Long(0);

        //on trouve quel est le player qui a le plus d'armées sur un territoire
        for (Map.Entry<Player, Long> armiesByPlayer : nbArmiesByPlayer.entrySet()) {
            //s'il a plus d'armées, on set le nouveau contrôleur
            newController = (armiesByPlayer.getValue() > most) ? (armiesByPlayer.getKey()) : (this.controller);
            //si le nombre d'armées est équivalent,le nouveau contrôleur est à null
            newController = (armiesByPlayer.getValue() == most) ? (null) : (newController);
            most = (armiesByPlayer.getValue() > most) ? (armiesByPlayer.getValue()) : (most);
        }
        /*        Long maxValueInMap = (Collections.max(nbArmiesByPlayer.values()));*/
        this.notifyAllObservers();
        return this.controller = newController;
    }

    public int countArmyByPlayer(Player player) {
        Map<Player, Long> nbArmiesByPlayer;
        nbArmiesByPlayer = this.armies.stream().collect(Collectors.groupingBy(a -> a.getPlayer(), Collectors.counting()));
        if (!nbArmiesByPlayer.containsKey(player))
            return 0;
        return nbArmiesByPlayer.get(player).intValue();
    }

    public void destroyCity() {
        this.city.notifyDestruction();
        this.city = null;
        this.controller = reevaluateController();
        this.notifyAllObservers();
    }

    @Override
    public void commit(Visitor v) {
        Integer id;
        if (this.id == null) {
            id = territoryMapper.insert(this);
            this.id = id;
        }
        else
            territoryMapper.update(this);
    }

    //----------------- Getters / setters -------------

    public Integer getId() {
        return id;
    }

    public void setId(Integer idTerretory) {
        this.id = idTerretory;
    }

    public void setType(TerritoryTypeEnum type) {
        this.type = type;
    }

    public Integer getMaxNumberArmies() {
        return maxNumberArmies;
    }

    public void setMaxNumberArmies(Integer maxNumberArmies) {
        this.maxNumberArmies = maxNumberArmies;
    }

    public City getCity() {
        return city;
    }

    public boolean setCity(City city, Player player) {
        this.city = city;
        this.controller = player;
        this.notifyAllObservers();
        return true;
    }

    public List<Army> getArmies() {
        return armies;
    }

    public void setArmies(List<Army> armies) {
        this.armies = armies;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public TerritoryTypeEnum getTerritoryType() {
        return type;
    }

    public Player getController() {
        return controller;
    }

    public void setController(Player controller) {
        this.controller = controller;
    }

	public GameMap getMap() {
		return map;
	}

	public void setMap(GameMap map) {
		this.map = map;
	}

	public TerritoryTypeEnum getType() {
		return type;
	}

	public void setCity(City city) {
		this.city = city;
	}
    
    
}
