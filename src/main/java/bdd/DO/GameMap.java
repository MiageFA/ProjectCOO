package bdd.DO;


import bdd.dao.mapper.GameMapMapper;
import model.ADomainObject;
import util.Visitor;

import java.util.HashMap;
import java.util.Map;

import static bdd.DO.TerritoryTypeEnum.*;

public class GameMap extends ADomainObject {

    private Integer idMap;
    private Integer width;
    private Integer height;
    private Integer maxArmiesPerTerritory;
    /* the id of map type */
    private Map<Coordinate, Territory> map;
    private GameMapMapper gameMapMapper;

    public GameMap() {
        gameMapMapper = GameMapMapper.getInstance().getInstance();
    }

    public GameMap(Integer width, Integer height, Integer maxArmiesPerTerritory) {
        gameMapMapper = GameMapMapper.getInstance();
        this.width = width;
        this.height = height;
        this.maxArmiesPerTerritory = maxArmiesPerTerritory;
        this.map = new HashMap<>();
        generateMap(width, height);
        notifyAllObservers();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idMap == null) ? 0 : idMap.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameMap other = (GameMap) obj;
        if (idMap == null) {
            if (other.idMap != null)
                return false;
        } else if (!idMap.equals(other.idMap))
            return false;
        return true;
    }


    public Territory getTerritory(Coordinate coordinate) {
        return this.map.get(coordinate);
    }

    /**
     * create territory by weighting the territory just above (coherence on map)
     * @param mountain
     * @param field
     * @param plain
     * @param x
     * @param y
     * @return new Territory
     */
    private Territory createTerritoryByWeight(double mountain, double field, double plain, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        if (mountain > field && mountain > plain)
            return new Territory(this.maxArmiesPerTerritory, MOUNTAIN, coordinate);
        else if (field > mountain && field > plain)
            return new Territory(this.maxArmiesPerTerritory, FIELD, coordinate);
        else
            return new Territory(this.maxArmiesPerTerritory, PLAIN, coordinate);
    }

    /**
     * Compute probabilityWeight of a territoryType
     * @param territoryType
     * @param ancestorType
     * @return
     */
    private double getWeight(TerritoryTypeEnum territoryType, TerritoryTypeEnum ancestorType) {
        if (territoryType == ancestorType)
            return (1.3);
        return 1;

    }

    /**
     * For each new territory, take the one just above if exists, and compute the type of territory to create
     * @param x
     * @param y
     * @return
     */
    private Territory createTerritory(int x, int y) {
        Territory territory;
        if (y <= 0)
            return createTerritoryByWeight(Math.random() * .50, Math.random(), Math.random(), x, y);
        Coordinate coordinate = new Coordinate(x, y - 1);
        territory = this.map.get(coordinate);
        return createTerritoryByWeight(Math.random() * .50 * getWeight(MOUNTAIN, territory.getTerritoryType()),
                Math.random() * getWeight(FIELD, territory.getTerritoryType()),
                Math.random() * getWeight(PLAIN, territory.getTerritoryType()), x, y);
    }

    private void generateMap(Integer width, Integer height) {
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
            	Territory territory =createTerritory(x, y);
            	territory.setMap(this);
                this.map.put(new Coordinate(x, y), territory);
            }
        notifyAllObservers();
    }

    public Integer getId() {
        return idMap;
    }

    public void setId(Integer idMap) {
        this.idMap = idMap;
    }

    public Map<Coordinate, Territory> getMap() {
        return map;
    }


    public Territory getTerritory(int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        return getTerritory(coordinate);
    }

    public void setMap(Map<Coordinate, Territory> map) {
        this.map = map;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public void commit(Visitor v) {
        Integer id;
        if (this.id == null)
        {
            id = gameMapMapper.insert(this);
            this.id = id;
        }
        else
            gameMapMapper.update(this);
    }
}
