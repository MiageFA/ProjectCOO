package bdd.DO;

import bdd.dao.mapper.CityMapper;
import model.ADomainObject;
import util.Visitor;

public class City extends ADomainObject {
    private String name;
    private Player player;
    private CityMapper cityMapper;

    public City() {
        cityMapper = CityMapper.getInstance();
    }

    public City(Player player) {
        super();
        this.cityMapper = CityMapper.getInstance();
        this.player = player;
        notifyAllObservers();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        City other = (City) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


    //--------------------------- getters/Setters -----------------------------
    public Integer getId() {
        return id;
    }

    public void setIdCity(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getPlayer() {
        return player;
    }


    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public void commit(Visitor v) {
        Integer id;
        if (this.id == null)
        {
            id = cityMapper.insert(this);
            this.id = id;
        }
        else
            cityMapper.update(this);
    }

}
