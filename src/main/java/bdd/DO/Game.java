package bdd.DO;


import bdd.dao.mapper.GameMapper;
import model.ADomainObject;
import service.RoundCalculatorService;
import service.gameService.InitCities;
import util.State;
import util.Visitor;

import java.util.*;

public class Game extends ADomainObject {
    private final Integer RESOURCE_PER_FIELD = 1;
    private final RoundCalculatorService roundCalculatorService = RoundCalculatorService.getInstance();

    private Player creator;
    private String name;
    private Date dateCreation;
    private Date dateBegin;
    private Date dateEnd;
    private Integer maxNumberPlayers;
    private State state;

    private Integer initNumberResourcesPerPlayer;
    private Integer numberResourcesPerRound;
    private Integer maxRounds;
    /*number cases between cities at the creation of the game*/
    private Integer initDistanceBetweenCities;
    private Integer roundDurationTime;
    /* id of the winner player */
    private Integer idWinner;
    private Round currentRound;

    private GameMap map;
    private Map<Player, Integer> resources = new HashMap<>();
    private List<Player> players = new ArrayList<>();
    private GameMapper gameMapper;


    public Game() {
        gameMapper = GameMapper.getInstance();
        
    }

    public Game(Integer maxNumberPlayers, Integer maxRounds, Integer initNumberResourcesPerPlayer, Integer numberResourcesPerRound,
                Integer roundDurationTime, String name, Player player, Integer height, Integer width, Integer maxArmiesPerTerritory, Player creator)
    {
        gameMapper = GameMapper.getInstance();
        this.maxNumberPlayers = maxNumberPlayers;
        this.maxRounds = maxRounds;
        this.initNumberResourcesPerPlayer = initNumberResourcesPerPlayer;
        this.numberResourcesPerRound = numberResourcesPerRound;
        this.roundDurationTime = roundDurationTime;
        this.name = name;
        this.players.add(player);
        this.state = State.IN_PROGRESS;
        this.map = new GameMap(width, height, maxArmiesPerTerritory);
        this.creator = creator;
        notifyAllObservers();
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Game other = (Game) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public void initCities()
    {
        InitCities initCities = new InitCities();
        initCities.initCities(map, players, initDistanceBetweenCities);
    }


    public void addResourcesByPlayer(Player player, Integer ressources) {
        this.resources.put(player, this.resources.get(player) + ressources);
        notifyAllObservers();
    }

    public boolean addPlayer(final Player player) {
        if (!canParticipate(player))
            return false;
        this.players.add(player);
        this.resources.put(player, initNumberResourcesPerPlayer);
        notifyAllObservers();
        return true;
    }

    public boolean canParticipate(Player player) {
        return (!players.contains(player) && (players.size() < maxNumberPlayers) && state == State.WAITING);
    }

    @Override
    public void commit(Visitor v) {
        Integer id;
        if (this.id == null)
        {
            id = gameMapper.insert(this);
            this.id = id;
        }
        else
            gameMapper.update(this);
    }

    public void generateMap(Integer x, Integer y, Integer maxNumberPlayers)
    {
        this.maxNumberPlayers = maxNumberPlayers;
        this.map = new GameMap(x, y, maxNumberPlayers);
        notifyAllObservers();
    }

    public void subResourcesByPlayer(Player player, Integer ressources) {
        this.resources.put(player, this.resources.get(player) - ressources);
        notifyAllObservers();
    }

    public void updateCurrentRound(Round currentRound)
    {
        this.setCurrentRound(currentRound);
        this.players.forEach(player -> player.addRessources(numberResourcesPerRound, this));
        this.map.getMap().forEach(((coordinate, territory) -> {
            if (TerritoryTypeEnum.FIELD.equals(territory.getTerritoryType()) && territory.getController() != null)
                territory.getController().addRessources(RESOURCE_PER_FIELD, this);
        }));
    }

    public void updateResource(final Player player, final Integer resources) {

        this.resources.put(player, resources);
        notifyAllObservers();
    }

    //------------------------------------------- Getters/Setters -------------------------------------------

    public Date getDateBegin() {
        return dateBegin;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public Integer getId() {
        return id;
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public Integer getInitNumberResourcesPerPlayer() {
        return initNumberResourcesPerPlayer;
    }

    public void setInitDistanceBetweenCities(Integer initDistanceBetweenCities) {
        this.initDistanceBetweenCities = initDistanceBetweenCities;
    }

    public Integer getInitDistanceBetweenCities() {
        return initDistanceBetweenCities;
    }

    public Integer getNumberResourcesPerRound() {
        return numberResourcesPerRound;
    }

    public Integer getMaxNumberPlayers() {
        return maxNumberPlayers;
    }

    public Integer getRoundDurationTime() {
        return roundDurationTime;
    }

    public void setNumberResourcesPerRound(Integer numberResourcesPerRound) {
        this.numberResourcesPerRound = numberResourcesPerRound;
    }

    public Integer getWinner() {
        return idWinner;
    }

    public void setId(Integer idGame) {
        this.id = idGame;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public void setMaxNumberPlayers(Integer maxNumberPlayers) {
        this.maxNumberPlayers = maxNumberPlayers;
    }

    public void setInitNumberResourcesPerPlayer(Integer initNumberResourcesPerPlayer) {
        this.initNumberResourcesPerPlayer = initNumberResourcesPerPlayer;
    }

    public void setRoundDurationTime(Integer roundDurationTime) {
        this.roundDurationTime = roundDurationTime;
    }

    public void setWinner(Integer winner) {
        this.idWinner = winner;
    }

    public void setCurrentRound(Round currentRound) {
        this.currentRound = currentRound;
        notifyAllObservers();
    }


    public Integer getIdWinner() {
        return idWinner;
    }

    public void setIdWinner(Integer idWinner) {
        this.idWinner = idWinner;
        notifyAllObservers();
    }

    public GameMap getMap() {
        return map;
    }

    public void setMap(GameMap map) {
        this.map = map;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Map<Player, Integer> getResources() {
        return resources;
    }

    public void setResources(Map<Player, Integer> resources) {
        this.resources = resources;
    }

    public Integer getResourcesByPlayer(Player player) {
        return resources.get(player);
    }

    public void setPlayersDO(List<Player> playersDO) {
        this.players = playersDO;
    }

    public State getState() {
        return state;
    }

    public Integer getMaxRounds() {
		return maxRounds;
	}

	public void setMaxRounds(Integer maxRounds) {
		this.maxRounds = maxRounds;
	}

	public void setState(State state) {
		this.state = state;
		notifyAllObservers();
	}

    public Player getCreator() {
        return creator;
    }

    public void setCreator(Player creator) {
        this.creator = creator;
    }


    @Override
    public String toString() {
        return this.getName() + " " + this.getDateCreation();
    }
}
