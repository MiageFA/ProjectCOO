package bdd.DO;

import bdd.dao.mapper.RoundMapper;
import model.ADomainObject;
import util.Visitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Round extends ADomainObject {


    private List<Player> players;
    private Integer id;
    private Integer roundNumber;
    private Date dateBegin;
    private Date dateEnd;
    private Game game;
    
    RoundMapper mapper = RoundMapper.getInstance();
    
    public Round(){
        players = new ArrayList<>();
    	notifyAllObservers();
    }
    @Override
    public void commit(Visitor v) {
    	
        Integer id;
        if (this.id == null)
        {
            id = mapper.insert(this);
            this.id = id;
        }
        else
            mapper.update(this);
    }
    
    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getRoundNumber() {
        return roundNumber;
    }


    public void setRoundNumber(Integer roundNumber) {
        this.roundNumber = roundNumber;
    }


    public Date getDateBegin() {
        return dateBegin;
    }


    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }


    public Date getDateEnd() {
        return dateEnd;
    }


    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }


    public List<Player> getPlayers() {
        return players;
    }


    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player)
    {
        this.players.add(player);
    }

	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}
	
	

}
