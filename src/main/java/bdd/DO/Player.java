package bdd.DO;

import bdd.dao.mapper.PlayerMapper;
import model.ADomainObject;
import util.Visitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player extends ADomainObject implements Serializable {
    private Integer id;
    private String firstName;
    private String lastName;
    private String pseudo;
    private String password;

    private List<Army> armyList;
    private List<City> cityList;

    private List<Game> games;

    private PlayerMapper playerMapper;

    public Player() {
        playerMapper = PlayerMapper.getInstance();
        armyList = new ArrayList<>();
        cityList = new ArrayList<>();
        games = new ArrayList<>();
    }

    public Player(String nickName, String firstName, String lastName, String password) {
        super();
        this.pseudo = nickName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;

        playerMapper = PlayerMapper.getInstance();
        this.armyList = new ArrayList<>();
        this.cityList = new ArrayList<>();
        notifyAllObservers();
    }

    public Player(String nickName, int defaultRessources, List<Army> armies, List<City> cities) {
        super();
        playerMapper = PlayerMapper.getInstance();
        this.armyList = armies;
        this.cityList = cities;
    }

    public List<Army> getArmyList() {
        return armyList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public boolean canPay(Integer price, Game game) {
        return ((game.getResourcesByPlayer(this) - price) > 0);
    }

    public void pay(Integer price, Game game) {
        game.subResourcesByPlayer(this, price);
        this.notifyAllObservers();
    }

    public void addRessources(Integer sum, Game game) {
        game.addResourcesByPlayer(this, sum);
        this.notifyAllObservers();
    }

    public boolean addArmy(Army army)
    {
        return armyList.add(army);
    }

    public boolean addCity(City city)
    {
        return cityList.add(city);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public void commit(Visitor v) {
        Integer id;
        if (this.id == null)
        {
            id = playerMapper.insert(this);
            this.id = id;
        }
        else
            playerMapper.update(this);
    }

    @Override
    public String toString() {
        return "player:[id:"+this.id+",pseudo:"+this.pseudo+",firstName:"+this.firstName+",last_name:"+this.lastName+"]";
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer idPlayer) {
        this.id = idPlayer;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }


}
