drop table if exists player;
create table player(
	id INT NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255),
	last_name VARCHAR(255),
	pseudo VARCHAR(255),
	password VARCHAR(255),
	primary key(id)
);

drop table if exists city;
create table city(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	id_player INT NOT NULL,

	primary key(id),
	foreign key(id_player) references palyer(id)
);

drop table if exists army;
create table army(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	id_territory int NOT NULL,
	id_player INT NOT null,

	primary key(id),
	foreign key(id_player) references player(id),
	foreign key(id_territory) references territory(id)
);

drop table if exists coordinate;
create table coordinate (
	id INT NOT NULL AUTO_INCREMENT,
	x INT NOT NULL,
	y INT NOT null,

	primary key(id)
);

drop table if exists state;
create table state(
	id Int not null,
	state VARCHAR(255),
	primary key(id)

);

drop table if exists game;
create table game(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	id_state INT,
	date_creation DATETIME,
	date_begin DATETIME,
	date_end DATETIME,
	max_number_players INT,
	init_number_resources_per_player INT,
	number_resources_per_round INT,
	init_distance_between_cities INT, /*number cases between cities at the creation of the game*/
	round_duration_time INT, /* number secondes to modify */
	player_winner INT, -- id player
	id_map INT,
	player_creator INT, -- the creator of this game 

	primary key(id),
	foreign key(id_state) references state(id),
	foreign key(player_winner) references player(id),
	foreign key(player_creator) references player(id),
	foreign key(id_map) references map(id)
);

drop table if exists map;
create table map(
	id INT NOT NULL AUTO_INCREMENT,
	width DOUBLE,
	height DOUBLE,
	primary key(id)
	);


drop table if exists territory_type;
create table territory_type(
	id INT NOT NULL,
	type VARCHAR (255),
	primary key(id)
);

drop table if exists territory;
create table territory(
	id INT NOT NULL AUTO_INCREMENT,
	height DOUBLE,
	width DOUBLE,
	id_territory_type INT,
	id_city INT,
	max_number_armies INT,
	id_coordinate INT,
	id_map INT,

	primary key (id),
	foreign key(id_territory_type) references territory_type(id),
	foreign key(id_city) references city(id),
	foreign key(id_coordinate) references coordinate(id),
	foreign key(id_map) references map(id)
);


drop table if exists player_in_game;
create table player_in_game(
	id_player INT NOT NULL,
	id_game INT NOT NULL,
	resources INT,
	primary key(id_player,id_game),
	foreign key(id_player) references player(id),
	foreign key(id_game) references game(id)
);

drop table if exists round;
create table round(
		id INT NOT NULL AUTO_INCREMENT,
		round_number INT, -- the round number in the game
		date_begin DATETIME NOT NULL,
		date_end DATETIME,
		id_game INT,
		primary key(id),
		foreign key(id_game) references game(id)
);

drop table if exists player_in_round;
create table player_in_round(
		id_player INT,
		id_round INT,
		primary key (id_player,id_round),
		foreign key (id_player) references player(id),
		foreign key (id_round) references round(id)
);

    
insert into territory_type(id,type) values (1,"MONTAIN");
insert into territory_type(id,type) values (2,"FIELD");
insert into territory_type(id,type) values (3,"PLAIN");

insert into state(id,state) values(1,"WAITING");
insert into state(id,state) values(2,"IN_PROGRESS");
insert into state(id,state) values(3,"FINISHED");
