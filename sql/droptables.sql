drop table if exists player;
drop table if exists city;
drop table if exists army;
drop table if exists coordinate;
drop table if exists game;
drop table if exists map;
drop table if exists player_in_game;
drop TABLE if EXISTS player_in_round;
drop TABLE if EXISTS round;
drop TABLE if EXISTS state;
drop TABLE if EXISTS territory;
drop TABLE if EXISTS territory_type;