**README**

**Equipe : Christophe Maingard (pas d'accès moodle), Koceila Ouikene**

**Prérequis**
<p>Afin de lancer l'éxecutable, prière d'avoir un serveur mysql prêt à l'emploi.</p>
<p>Un connecteur mysql, téléchargeable à cette addresse (https://dev.mysql.com/downloads/connector/) devra ajouté à la bibliothèque du projet.
L'utilisateur par défaut utilisé sera 'root':'';
Ces informations sont disponibles dans la classe DBConnection.java où il est possible de les y modifier si besoin.
</p>

**Utilisation**
<p>Lancer une base mySql et l'appeler 'projetCOO', puis importer et éxecuter le script 'table.sql'.</p>